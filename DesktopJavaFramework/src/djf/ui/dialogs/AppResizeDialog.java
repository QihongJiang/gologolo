/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package djf.ui.dialogs;

import static djf.AppPropertyType.APP_BANNER;
import static djf.AppPropertyType.APP_LOGO;
import static djf.AppPropertyType.APP_PATH_IMAGES;
import static djf.AppPropertyType.APP_RESIZE;
import static djf.AppPropertyType.APP_RESIZE_DIMENSION;
import static djf.AppPropertyType.CLASS_LOGO_DIALOG_PROMPT;
import static djf.AppPropertyType.CLASS_LOGO_DIALOG_TEXT_FIELD;
import static djf.AppPropertyType.LOAD_ERROR_CONTENT;
import static djf.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.AppPropertyType.LOGO_ITEM_DIALOG_CANCEL_BUTTON_TEXT;
import static djf.AppPropertyType.LOGO_ITEM_DIALOG_HEIGHT_PROMPT;
import static djf.AppPropertyType.LOGO_ITEM_DIALOG_OK_BUTTON_TEXT;
import static djf.AppPropertyType.LOGO_ITEM_DIALOG_WIDTH_PROMPT;
import static djf.AppPropertyType.LOGO_RESIZE_DIALOG_CANCEL_BUTTON;
import static djf.AppPropertyType.LOGO_RESIZE_DIALOG_CANCEL_BUTTON_TEXT;
import static djf.AppPropertyType.LOGO_RESIZE_DIALOG_OK_BUTTON;
import static djf.AppPropertyType.LOGO_RESIZE_DIALOG_OK_BUTTON_TEXT;
import static djf.AppPropertyType.NEW_SUCCESS_CONTENT;
import static djf.AppPropertyType.NEW_SUCCESS_TITLE;
import static djf.AppPropertyType.RESIZE_DIALOG_HEIGHT;
import static djf.AppPropertyType.RESIZE_DIALOG_WIDTH;
import static djf.AppPropertyType.RESIZE_HEADER_TEXT;
import static djf.AppPropertyType.WELCOME_DIALOG_NEW_BUTTON_TEXT;
import static djf.AppPropertyType.WELCOME_DIALOG_RECENT_WORK_LABEL;
import static djf.AppPropertyType.WELCOME_DIALOG_TITLE;
import djf.AppTemplate;
import djf.modules.AppLanguageModule;
import static djf.modules.AppLanguageModule.FILE_PROTOCOL;
import djf.modules.AppRecentWorkModule;
import static djf.ui.style.DJFStyle.CLASS_DJF_DIALOG_PROMPT;
import static djf.ui.style.DJFStyle.CLASS_DJF_DIALOG_TEXT_FIELD;
import static djf.ui.style.DJFStyle.CLASS_DJF_WELCOME_BANNER;
import static djf.ui.style.DJFStyle.CLASS_DJF_WELCOME_HEADER;
import static djf.ui.style.DJFStyle.CLASS_DJF_WELCOME_NEW_BUTTON;
import static djf.ui.style.DJFStyle.CLASS_DJF_WELCOME_NEW_PANE;
import static djf.ui.style.DJFStyle.CLASS_DJF_WELCOME_RECENT_BUTTON;
import static djf.ui.style.DJFStyle.CLASS_DJF_WELCOME_RECENT_PANE;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import properties_manager.PropertiesManager;

/**
 *
 * @author Qihong
 */
public class AppResizeDialog extends Stage{
    AppTemplate app;
    // LEFT PANE
    VBox recentlyEditedPane;
    Label recentWorkLabel;
    ArrayList<Button> recentWorkButtons;
    
    // RIGHT PANE
    BorderPane splashPane;
    ImageView resizeDialogImageView;
    HBox newPane;
    GridPane VPane;
    Button okButton;
    Button cancelButton;
    Label heightLabel = new Label();
    TextField heightTextField = new TextField();
    Label widthLabel = new Label();
    TextField widthTextField = new TextField();
    public double height;
    public double width;
    
    String selectedPath = null;
    String selectedWorkName = null;

    public AppResizeDialog() {
       
        // WE'LL NEED THIS
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        splashPane = new BorderPane();
        resizeDialogImageView = new ImageView();
        try {
            String bannerFileName1 = props.getProperty(APP_RESIZE);
            
            String bannerPath1 = props.getProperty(APP_PATH_IMAGES) + "/" + bannerFileName1;
            
            File bannerFile1 = new File(bannerPath1);
            
            BufferedImage bufferedImage1 = ImageIO.read(bannerFile1);
            
            Image bannerImage1 = SwingFXUtils.toFXImage(bufferedImage1, null);
            
            resizeDialogImageView.setImage(bannerImage1);
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }        
        String okText = props.getProperty(LOGO_RESIZE_DIALOG_OK_BUTTON_TEXT);
        String cancelText = props.getProperty(LOGO_RESIZE_DIALOG_CANCEL_BUTTON_TEXT);
        okButton = new Button(okText);
        okButton.setOnAction(e->{
            height = Double.parseDouble(heightTextField.getText());
            width = Double.parseDouble(widthTextField.getText());
            this.hide();
        });
        cancelButton = new Button(cancelText);
        cancelButton.setOnAction(e->{
            this.hide();
        });
        newPane = new HBox();
        VPane = new GridPane();
        
        Label label = new Label(props.getProperty(RESIZE_DIALOG_HEIGHT));
        
        VPane.add(label, 1, 0);
        VPane.add(heightTextField, 2, 0);
        VPane.add(new Label(props.getProperty(RESIZE_DIALOG_WIDTH)), 1, 1);
        VPane.add(widthTextField, 2, 1);
        newPane.setAlignment(Pos.CENTER);
        newPane.getChildren().add(okButton);
        newPane.getChildren().add(cancelButton);
        splashPane.setTop(resizeDialogImageView);
        splashPane.setCenter(VPane);
        splashPane.setBottom(newPane);
        
        // WE ORGANIZE EVERYTHING IN HERE
        BorderPane dialogPane = new BorderPane();
        dialogPane.setCenter(splashPane);

        // MAKE AND SET THE SCENE
        Scene dialogScene = new Scene(dialogPane);
        this.setScene(dialogScene);

        // PUT EVERYTHING IN THE DIALOG WINDOW
        String dialogTitle = props.getProperty(RESIZE_HEADER_TEXT);
        this.setTitle(dialogTitle);
        
        // SET THE APP ICON
        String imagesPath = props.getProperty(APP_PATH_IMAGES);
        String appLogo = FILE_PROTOCOL + imagesPath + props.getProperty(APP_LOGO);
        Image appWindowLogo = new Image(appLogo);
        this.getIcons().add(appWindowLogo);

        // SPECIFY THE STYLE THE BANNER AND NEW BUTTON
        
        resizeDialogImageView.getStyleClass().add(CLASS_DJF_WELCOME_BANNER);
        newPane.getStyleClass().add(CLASS_DJF_WELCOME_NEW_PANE);
        VPane.getStyleClass().add(CLASS_DJF_DIALOG_TEXT_FIELD);
        VPane.getStyleClass().add(CLASS_DJF_DIALOG_PROMPT);
        okButton.getStyleClass().add(CLASS_DJF_WELCOME_NEW_BUTTON);
    }
    
    public double getHeightLength(){
        return height;
    }
    
    public double getWidthLength(){
        return width;
    }
}
