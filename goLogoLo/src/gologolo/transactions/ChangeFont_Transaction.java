/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoText;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_TEXT_BOLD;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_TEXT_ITALIC;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class ChangeFont_Transaction implements jTPS_Transaction{
    GoLogoLoData data;
    String font;
    Font fonts;
    GoLogoLoText textField;
    Font tempFont;
    
    public ChangeFont_Transaction (GoLogoLoData initData, String initFont){
        font = initFont;
        data = initData;
        int index = data.getItems().indexOf(data.getSelectedItem());
        textField = (GoLogoLoText) data.getNode(index);
        tempFont = textField.getFont();
    }
    
    @Override
    public void doTransaction() {
        if(data.getSelectedItem().getIsText()){
            textField.setFont(font);
            fonts = new Font(font, textField.getSize());
            if(textField.isBold()){
                textField.setFont(Font.font(font, FontWeight.BOLD, textField.getSize()));
            }
            else if(textField.isItalic()){
                textField.setFont(Font.font(font, FontPosture.ITALIC, textField.getSize()));
            }
            else{
                textField.setFont(fonts);
            }
        }
    }

    @Override
    public void undoTransaction() {
        textField.setFont(tempFont);
        textField.setFont(tempFont.toString());
    }
    
}
