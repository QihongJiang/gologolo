/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;
import djf.ui.AppNodesBuilder;
import gologolo.GoLogoLoApp;
import jtps.jTPS_Transaction;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoItemPrototype;
import gologolo.data.GoLogoLoText;
import gologolo.workspace.controller.ItemsController;
import gologolo.workspace.dialog.GoLogoLoDialog;
import gologolo.workspace.dialog.GoLogoLoTextField;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_TEXT_FIELD;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

/**
 *
 * @author McKillaGorilla
 */
public class AddText_Transaction implements jTPS_Transaction {
    String s;
    GoLogoLoText text;
    GoLogoLoApp app;
    GoLogoLoData data;
    GoLogoLoItemPrototype itemToAdd;
    GoLogoLoDialog logoText;
    
    public AddText_Transaction(GoLogoLoData initData, GoLogoLoItemPrototype initNewItem, GoLogoLoDialog logoText) {
        data = initData;
        itemToAdd = initNewItem;
        this.logoText = logoText;
    }

    @Override
    public void doTransaction() {
        itemToAdd.setIsText(true);
        itemToAdd.setIsShape(false);
        itemToAdd.setIsCircle(false);
        itemToAdd.setIsImage(false);
        data.addItem(itemToAdd);
        s = logoText.getText();
        text = new GoLogoLoText(s,10,10);
        text.setFont("Arial");
        text.setTxt(s);
        text.setText(s);
        text.setX(20);
        text.setY(20);
        data.addNode(text);
    }

    @Override
    public void undoTransaction() {
        data.removeItem(itemToAdd);
        data.removeNode(text);
    }
}
