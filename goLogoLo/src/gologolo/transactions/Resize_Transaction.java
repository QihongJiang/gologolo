/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import static djf.AppPropertyType.LOGO_RESIZE_DIALOG_OK_BUTTON_TEXT;
import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.LOGO_MIDDLE_PANE;
import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class Resize_Transaction implements jTPS_Transaction{
    GoLogoLoApp app;
    Pane pane;
    double height, width;
    
    public Resize_Transaction(GoLogoLoApp initApp, double initHeight, double initWidth){
        app = initApp;
        height = initHeight;
        width = initWidth;
    }
    @Override
    public void doTransaction() {
        pane = (Pane) app.getGUIModule().getGUINode(LOGO_MIDDLE_PANE);
        pane.setMaxSize(width, height);
    }

    @Override
    public void undoTransaction() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
