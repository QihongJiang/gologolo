/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.CYCLE_METHOD;
import gologolo.data.GoLogoLoCircle;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoRectangle;
import javafx.scene.control.ComboBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class Cycle_Transaction implements jTPS_Transaction{
    RadialGradient gradient;
    String cycle;
    CycleMethod prevCycle;
    ComboBox cycleMethod;
    GoLogoLoApp app;
    GoLogoLoRectangle rec;
    GoLogoLoCircle cir;
    GoLogoLoData data;
    public Cycle_Transaction(GoLogoLoApp initApp, String value){
        app = initApp;
        cycle = value;
        data = (GoLogoLoData) app.getDataComponent();
        if(data.getSelectedItem().isIsCircle()){
            cir = (GoLogoLoCircle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevCycle = cir.getCycleMethod();
        }
        else{
            rec = (GoLogoLoRectangle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevCycle = rec.getCycleMethod();
        }
        cycleMethod = (ComboBox) app.getGUIModule().getGUINode(CYCLE_METHOD);
    }
    
    @Override
    public void doTransaction() {
        if(data.getSelectedItem().getIsShape()){
            prevCycle = rec.getCycleMethod();
            gradient = new RadialGradient(rec.getFocusAngle(), rec.getFocusDistance(), 
                                          rec.getCenterX(), rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), CycleMethod.valueOf(cycle), rec.getStop0(), rec.getStop1());
            
            rec.setCycleMethod(CycleMethod.valueOf(cycle));
            cycleMethod.setValue(cycle);
            rec.setFill(gradient);
        }
        else{
            gradient = new RadialGradient(cir.getFocusAngle(), cir.getFocusDistance(), 
                                          cir.getRadialX(), cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), CycleMethod.valueOf(cycle), cir.getStop0(), cir.getStop1());
            cir.setCycleMethod(CycleMethod.valueOf(cycle));
            cycleMethod.setValue(cycle);
            cir.setFill(gradient);
        }
    }

    @Override
    public void undoTransaction() {
        if(data.getSelectedItem().getIsShape()){
                rec.setFill(new RadialGradient(rec.getFocusAngle(), rec.getFocusDistance(), 
                                          rec.getCenterX(), rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), prevCycle, rec.getStop0(), rec.getStop1()));
                cycleMethod.setValue(prevCycle);
                rec.setCycleMethod(prevCycle);
        }
        else{
            cir.setFill(new RadialGradient(cir.getFocusAngle(), cir.getFocusDistance(), 
                                          cir.getRadialX(), cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), prevCycle, cir.getStop0(), cir.getStop1()));
                cycleMethod.setValue(prevCycle);
                cir.setCycleMethod(prevCycle);
        }
    }
}
