/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoImage;
import gologolo.data.GoLogoLoItemPrototype;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import javafx.scene.image.Image;
        
import javafx.scene.image.ImageView;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class AddImage_Transaction implements jTPS_Transaction{
    GoLogoLoData data;
    File fileToEdit;
    Image image;
    ImageView imageView;
    GoLogoLoImage logoImage;
    GoLogoLoItemPrototype itemToAdd;
    
    public AddImage_Transaction(GoLogoLoData initData, File file, GoLogoLoItemPrototype initNewItem) throws FileNotFoundException{
        data=initData;
        InputStream location = new FileInputStream(file);
        image = new Image(location);
        itemToAdd = initNewItem;
    }
    
    @Override
    public void doTransaction() {
        itemToAdd.setIsCircle(false);
        itemToAdd.setIsImage(true);
        itemToAdd.setIsShape(false);
        itemToAdd.setIsText(false);
        data.addItem(itemToAdd);
        imageView = new ImageView(image);
        logoImage = new GoLogoLoImage(imageView, 200, 200);
        data.addImage(logoImage.getImageV());
    }

    @Override
    public void undoTransaction() {
        data.removeItem(itemToAdd);
        data.removeNode(logoImage.getImageV());
    }
}
