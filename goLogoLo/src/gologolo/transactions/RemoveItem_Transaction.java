/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import java.util.ArrayList;
import jtps.jTPS_Transaction;
import static djf.AppPropertyType.APP_CLIPBOARD_FOOLPROOF_SETTINGS;
import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoItemPrototype;
import javafx.scene.Node;
/**
 *
 * @author qihong
 */

public class RemoveItem_Transaction implements jTPS_Transaction {
    Node tempNode;
    int count;
    GoLogoLoApp app;
    ArrayList<GoLogoLoItemPrototype> itemsToRemove;
    ArrayList<Integer> removedItemLocations;
    
    public RemoveItem_Transaction(GoLogoLoApp initApp, ArrayList<GoLogoLoItemPrototype> initItems, int initCount) {
        count = initCount;
        app = initApp;
        itemsToRemove = initItems;
    }

    @Override
    public void doTransaction() {
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if(data.isItemSelected()){
        removedItemLocations = data.removeAll(itemsToRemove);
        tempNode = data.getNode(data.getItemIndex(data.getSelectedItem())+1);
        data.removeNode(data.getItemIndex(data.getSelectedItem())+1);
        }
    }

    @Override
    public void undoTransaction() {
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        data.addAll(itemsToRemove, removedItemLocations);
        data.addNode(tempNode);
    }
}

