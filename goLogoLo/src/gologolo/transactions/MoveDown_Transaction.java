/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoItemPrototype;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class MoveDown_Transaction implements jTPS_Transaction{
    GoLogoLoApp app;
    int newIndex;
    int oldIndex;
    GoLogoLoItemPrototype selectedItem;
    public MoveDown_Transaction(GoLogoLoApp initApp) {
        app = initApp;
    }

    @Override
    public void doTransaction() {
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        GoLogoLoItemPrototype item = data.getSelectedItem();
        newIndex = data.getItemIndex(item)+1;
        selectedItem = item;
        oldIndex = data.getItemIndex(item);
        data.moveItem(oldIndex, newIndex);
        data.moveNode(oldIndex, newIndex);
        data.clearSelected();
        data.selectItem(selectedItem);
    }

    @Override
    public void undoTransaction() {
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        data.moveItem(newIndex, oldIndex); 
        data.moveNode(newIndex, oldIndex);
        data.clearSelected();
        data.selectItem(selectedItem);
    }
}
