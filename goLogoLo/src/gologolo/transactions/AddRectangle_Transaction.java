/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoItemPrototype;
import gologolo.data.GoLogoLoRectangle;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_RECTANGLE;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;

import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class AddRectangle_Transaction implements jTPS_Transaction{
    GoLogoLoData data;
    GoLogoLoItemPrototype itemToAdd;
    GoLogoLoRectangle rec;
    
    public AddRectangle_Transaction(GoLogoLoData initData, GoLogoLoItemPrototype initNewItem){
        itemToAdd = initNewItem;
        data=initData;
    }
    
    @Override
    public void doTransaction() {
        itemToAdd.setIsText(false);
        itemToAdd.setIsShape(true);
        itemToAdd.setIsImage(false);
        itemToAdd.setIsCircle(false);
        data.addItem(itemToAdd);
        rec = new GoLogoLoRectangle(200.0, 100.0, 300, 300);
        rec.setFill(Color.WHITE);
        rec.setStroke(Color.BLACK);
        rec.setX(30);
        rec.setXPos(30);
        rec.setY(30);
        rec.setYPos(30);
        data.addNode(rec);
    }

    @Override
    public void undoTransaction() {
        data.removeNode(rec);
        data.removeItem(itemToAdd);
    }
}
