/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import static djf.AppPropertyType.APP_CLIPBOARD_FOOLPROOF_SETTINGS;
import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoItemPrototype;
import java.util.ArrayList;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class CutItems_Transaction implements jTPS_Transaction{
    GoLogoLoApp app;
    ArrayList<GoLogoLoItemPrototype> itemsToCut;
    ArrayList<Integer> cutItemLocations;
    ArrayList<Node> componentsToCut;
    ArrayList<Integer> cutComponentsLocations;
    Node tempNode;
    GoLogoLoData data;
    
    public CutItems_Transaction(GoLogoLoApp initApp, ArrayList<GoLogoLoItemPrototype> initItemsToCut, ArrayList<Node> initComponentsToCut) {
        app = initApp;
        itemsToCut = initItemsToCut;
        componentsToCut = initComponentsToCut;
        data = (GoLogoLoData)app.getDataComponent();
        tempNode = data.getNode(data.getItemIndex(data.getSelectedItem()));
    }

    @Override
    public void doTransaction() {
        cutComponentsLocations = data.removeAll(itemsToCut);
        data.removeNode(data.getItems().indexOf(data.getSelectedItem()));
    }

    @Override
    public void undoTransaction() {
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        data.addAll(itemsToCut, cutItemLocations);
        data.addNode(tempNode);
    }
}
