/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoCircle;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoImage;
import gologolo.data.GoLogoLoItemPrototype;
import gologolo.data.GoLogoLoRectangle;
import gologolo.data.GoLogoLoText;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_TEXT_FIELD;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_RECTANGLE;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_RECTANGLES;
import java.util.ArrayList;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.paint.RadialGradient;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class PasteItems_Transaction implements jTPS_Transaction {
    GoLogoLoApp app;
    ArrayList<GoLogoLoItemPrototype> itemsToPaste;
    ArrayList<Node> nodes;
    GoLogoLoText text;
    GoLogoLoText origText;
    GoLogoLoCircle origCircle;
    GoLogoLoImage origImage;
    GoLogoLoImage image;
    GoLogoLoRectangle rec;
    GoLogoLoRectangle origRec;
    GoLogoLoCircle cir;
    int pasteIndex;
    
    boolean copy;
    
    public PasteItems_Transaction(  GoLogoLoApp initApp, 
                                    ArrayList<GoLogoLoItemPrototype> initItemsToPaste,
                                    ArrayList<Node> initNodes,
                                    int initPasteIndex, 
                                    boolean initCopy) {
        app = initApp;
        itemsToPaste = initItemsToPaste;
        nodes = initNodes;
        pasteIndex = initPasteIndex;
        copy = initCopy;
    }

    @Override
    public void doTransaction() {
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        int index = pasteIndex+1;
        for (GoLogoLoItemPrototype itemToPaste : itemsToPaste) {
            GoLogoLoItemPrototype newItem = (GoLogoLoItemPrototype)itemToPaste.clone();
            data.addItemAt(newItem, index);
            if(newItem.getIsShape()){
                if(copy){
                    origRec = (GoLogoLoRectangle) nodes.get(data.getItems().indexOf(itemToPaste));
                }else{
                    origRec = (GoLogoLoRectangle) nodes.get(data.getItems().indexOf(itemToPaste)+1);
                }
                rec = (GoLogoLoRectangle) origRec.clone();         
                System.out.println(nodes);
                
                
                data.addNode(rec);
                System.out.println(nodes);
                rec.setStrokeWidth(rec.getBorderThickness());
                rec.setStroke(rec.getBorderColor());
                rec.setArcHeight(rec.getBorderRadius());
                rec.setArcWidth(rec.getBorderRadius());
                rec.setFill(new RadialGradient(rec.getFocusAngle(), rec.getFocusDistance(), 
                                          rec.getCenterX(), rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), rec.getStop1()));
                
            }
            else if(newItem.getIsText()){
                if(copy){
                    origText = (GoLogoLoText)nodes.get(data.getItems().indexOf(itemToPaste));
                }else{
                    origText = (GoLogoLoText)nodes.get(data.getItems().indexOf(itemToPaste)+1);
                }
                text = (GoLogoLoText) origText.clone();
                text.setText(origText.getText());
                text.setFill(text.getColor());
                text.setFont(text.getFont());
                data.addNode(text);
            }
            else if(newItem.isIsCircle()){
                if(copy){
                    origCircle = (GoLogoLoCircle)nodes.get(data.getItems().indexOf(itemToPaste));
                }else{
                    origCircle = (GoLogoLoCircle)nodes.get(data.getItems().indexOf(itemToPaste)+1);
                }
                
                cir = (GoLogoLoCircle) origCircle.clone();         
                System.out.println(nodes);
                data.addNode(cir);
                System.out.println(nodes);
                cir.setCenterX(origCircle.getCenterX());
                cir.setCenterY(origCircle.getCenterY());
                cir.setRadius(origCircle.getRadius());
                cir.setStrokeWidth(cir.getBorderThickness());
                cir.setStroke(cir.getBorderColor());
                cir.setFill(new RadialGradient(cir.getFocusAngle(), cir.getFocusDistance(), 
                                          cir.getRadialX(), cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), cir.getStop1()));
            }
            else{
                if(copy){
                    origImage = (GoLogoLoImage)nodes.get(data.getItems().indexOf(itemToPaste));
                }else{
                    origImage = (GoLogoLoImage)nodes.get(data.getItems().indexOf(itemToPaste)+1);
                }
                image = (GoLogoLoImage) origImage.clone();
                data.addNode(image);
            }
            index++;
        }
        
    }

    @Override
    public void undoTransaction() {
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        for (GoLogoLoItemPrototype itemToPaste : itemsToPaste) {
            data.removeItem(itemToPaste);
            if(itemToPaste.isIsCircle()){
                data.removeNode(cir);
            }
            else if(itemToPaste.getIsText()){
                data.removeNode(text);
            }
            else if(itemToPaste.getIsShape())
                data.removeNode(rec);
            else
                data.removeNode(image);
        }
    }   
}
