/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoText;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_TEXT_BOLD;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_TEXT_ITALIC;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import jtps.jTPS_Transaction;
/**
 *
 * @author qihong
 */
public class ChangeSize_Transaction implements jTPS_Transaction{
    GoLogoLoData data;
    double size;
    double oldSize;
    GoLogoLoText textField;
    Font oldFont;
    Font font;
    
    public ChangeSize_Transaction (GoLogoLoData initData, double initSize, GoLogoLoText initText){
        size = initSize;
        textField = initText;
        oldSize = textField.getSize();
        data = initData;
        
        textField = initText;
        oldFont = textField.getFont();
    }
    
    @Override
    public void doTransaction() {
        if(data.getSelectedItem().getIsText()){
            if(textField.isBold()){
                oldFont = Font.font(textField.getFontToString(), FontWeight.BOLD, oldSize);
                font = Font.font(textField.getFontToString(), FontWeight.BOLD, size);
            }
            else if(textField.isItalic()){
                oldFont = Font.font(textField.getFontToString(), FontWeight.BOLD, oldSize);
                font = Font.font(textField.getFontToString(), FontPosture.ITALIC, size);
            }
            else{
                oldFont = new Font(oldSize);
                font = new Font(size);
            }
            textField.setFont(font);
            textField.setSize(size);
        }
    }

    @Override
    public void undoTransaction() {
        textField.setFont(oldFont);
        textField.setSize(oldFont.getSize());
    }
}
