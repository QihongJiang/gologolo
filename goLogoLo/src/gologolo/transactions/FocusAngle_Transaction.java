/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.BORDER_THICKNESS;
import static gologolo.GoLogoLoPropertyType.FOCUS_ANGLE;
import gologolo.data.GoLogoLoCircle;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoRectangle;
import javafx.scene.control.Slider;
import javafx.scene.paint.RadialGradient;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class FocusAngle_Transaction implements jTPS_Transaction{
    RadialGradient gradient;
    double focusAngle;
    double prevFocusAngle;
    GoLogoLoApp app;
    GoLogoLoRectangle rec;
    GoLogoLoCircle cir;
    GoLogoLoData data;
    Slider slider;
    public FocusAngle_Transaction(GoLogoLoApp initApp, double value){
        app = initApp;
        focusAngle = value;
        slider = (Slider) app.getGUIModule().getGUINode(FOCUS_ANGLE);
        data = (GoLogoLoData) app.getDataComponent();
        if(data.getSelectedItem().isIsCircle()){
            cir = (GoLogoLoCircle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevFocusAngle = cir.getFocusAngle();
        }
        else{
            rec = (GoLogoLoRectangle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevFocusAngle = rec.getFocusAngle();
        }
    }
    
    @Override
    public void doTransaction() {
        if(data.getSelectedItem().getIsShape()){
            gradient = new RadialGradient(focusAngle, rec.getFocusDistance(), 
                                          rec.getCenterX(), rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), rec.getStop1());
            rec.setFocusAngle(focusAngle);
            slider.adjustValue(focusAngle);
            rec.setFill(gradient);
        }
        else{
            gradient = new RadialGradient(focusAngle, cir.getFocusDistance(), 
                                          cir.getRadialX(), cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), cir.getStop1());
            cir.setFocusAngle(focusAngle);
            slider.adjustValue(focusAngle);
            cir.setFill(gradient);
        }
    }

    @Override
    public void undoTransaction() {
        if(data.getSelectedItem().getIsShape()){
            rec.setFill(new RadialGradient(prevFocusAngle, rec.getFocusDistance(), 
                                          rec.getCenterX(), rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), rec.getStop1()));
            rec.setFocusAngle(prevFocusAngle);
        }
        else{
            cir.setFill(new RadialGradient(prevFocusAngle, cir.getFocusDistance(), 
                                          cir.getRadialX(), cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), cir.getStop1()));
            cir.setFocusAngle(prevFocusAngle);
        }
        slider.adjustValue(prevFocusAngle);
    }
    
}
