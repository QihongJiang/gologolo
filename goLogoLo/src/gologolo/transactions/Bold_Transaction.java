/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoItemPrototype;
import gologolo.data.GoLogoLoText;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_TEXT_BOLD;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class Bold_Transaction implements jTPS_Transaction{
    GoLogoLoApp app;
    GoLogoLoItemPrototype selectedItem;
    GoLogoLoText text;
    String font;
    Font tempFont;
    
    public Bold_Transaction(GoLogoLoApp initApp) {
        app = initApp;
    }

    @Override
    public void doTransaction() {
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        GoLogoLoItemPrototype item = data.getSelectedItem();
        if(item.getIsText()){
            text = (GoLogoLoText) data.getNode(data.getItems().indexOf(item));
            text.setBold(true);
            text.setItalic(false);
            text.setFont(Font.font(text.getFontToString(), FontWeight.BOLD, text.getSize()));
        }
    }

    @Override
    public void undoTransaction() {
        text.setFont(Font.font(text.getFontToString(), FontWeight.BOLD, text.getSize()));
    }
}
