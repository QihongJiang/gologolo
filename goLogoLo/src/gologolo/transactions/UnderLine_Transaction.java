/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoItemPrototype;
import gologolo.data.GoLogoLoText;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class UnderLine_Transaction implements jTPS_Transaction{
    GoLogoLoApp app;
    GoLogoLoItemPrototype selectedItem;
    GoLogoLoItemPrototype item;
    GoLogoLoText text;
    Color color;
    Color prevColor;
    GoLogoLoData data;
    
    public UnderLine_Transaction(GoLogoLoApp initApp, Color value) {
        app = initApp;
        color = value;
        data = (GoLogoLoData)app.getDataComponent();
        item = data.getSelectedItem();
        text = (GoLogoLoText) data.getNode(data.getItems().indexOf(item));
        prevColor = text.getColor();
    }

    @Override
    public void doTransaction() {
        if(item.getIsText()){
            text.setFill(color);
            text.setColor(color);
        }
    }

    @Override
    public void undoTransaction() {
        text.setFill(prevColor);
    }
}
