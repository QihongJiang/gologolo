/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoItemPrototype;
import gologolo.data.GoLogoLoText;
import gologolo.workspace.dialog.GoLogoLoTextField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class EditTextField_Transaction implements jTPS_Transaction{
    GoLogoLoData data;
    GoLogoLoItemPrototype itemToEdit;
    GoLogoLoItemPrototype dataTemp;
    TextField textField;
    GoLogoLoText text;
    String oldText;
    GoLogoLoTextField logoText;
    String newText;
    String prevText;
    Pane pane;
    int count;
    
    public EditTextField_Transaction(GoLogoLoData initData, GoLogoLoTextField logoText, int initCount){
        count = initCount;
        data = initData;
        this.logoText = logoText;
    }

    @Override
    public void doTransaction() {
       newText = logoText.getText();
       text = (GoLogoLoText) data.getSelectedComponents().get(data.getItemIndex(data.getSelectedItem()));
       oldText = text.getText();
       text.setText(newText);
    }

    @Override
    public void undoTransaction() {
        text.setText(oldText);
    }
}
