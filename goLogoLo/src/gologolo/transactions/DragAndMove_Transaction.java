/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoRectangle;
import gologolo.data.GoLogoLoText;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class DragAndMove_Transaction implements jTPS_Transaction{
    Node node;
    GoLogoLoApp app;
    GoLogoLoText text;
    GoLogoLoRectangle rect;
    double origX, origY;
    double newX, newY; 
    public DragAndMove_Transaction(Node initNode, double offsetX, double offsetY, double newTranslateX, double newTranslateY) {
       node = initNode;
       origX = offsetX;
       origY = offsetY;
       newX = newTranslateX;
       newY = newTranslateY;
    }

    @Override
    public void doTransaction() {
        node.setTranslateX(newX);
        node.setTranslateY(newY);
    }

    @Override
    public void undoTransaction() {
        node.setTranslateX(origX);
        node.setTranslateY(origY);
    }
}
