/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.STOP_0;
import static gologolo.GoLogoLoPropertyType.STOP_1;
import gologolo.data.GoLogoLoCircle;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoRectangle;
import javafx.scene.control.ColorPicker;
import javafx.scene.paint.Color;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class Stop1_Transaction implements jTPS_Transaction{
    RadialGradient gradient;
    Color color;
    Color prevColor;
    Stop stop;
    Stop prevStop;
    GoLogoLoApp app;
    GoLogoLoData data;
    GoLogoLoRectangle rec;
    GoLogoLoCircle cir;
    ColorPicker colorPicker;
    public Stop1_Transaction(GoLogoLoApp initApp, Color value){
        app = initApp;
        color = value;
        stop = new Stop(1, color);
        data = (GoLogoLoData) app.getDataComponent();
        if(data.getSelectedItem().getIsShape()){
            rec = (GoLogoLoRectangle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevStop = rec.getStop1();
        }
        else{
            cir = (GoLogoLoCircle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevStop = cir.getStop1();
        }
        colorPicker = (ColorPicker) app.getGUIModule().getGUINode(STOP_1);
    }
    
    @Override
    public void doTransaction() {
        if(data.getSelectedItem().getIsShape()){
            if(data.getSelectedItem().getIsShape()){
            gradient = new RadialGradient(rec.getFocusAngle(), rec.getFocusDistance(), 
                                          rec.getCenterX(), rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), stop);
            rec.setStop1(stop);
            colorPicker.setValue(stop.getColor());
            rec.setFill(gradient);
        }
        }
        else{
            gradient = new RadialGradient(cir.getFocusAngle(), cir.getFocusDistance(), 
                                          cir.getRadialX(), cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), stop);
            cir.setStop1(stop);
            colorPicker.setValue(stop.getColor());
            cir.setFill(gradient);
        }
    }

    @Override
    public void undoTransaction() {
        if(data.getSelectedItem().getIsShape()){
        rec.setFill(new RadialGradient(rec.getFocusAngle(), rec.getFocusDistance(), 
                                          rec.getCenterX(), rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), prevStop));
        colorPicker.setValue(prevStop.getColor());
        rec.setStop1(stop);
        }
        else{
            gradient = new RadialGradient(cir.getFocusAngle(), cir.getFocusDistance(), 
                                          cir.getRadialX(), cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), stop);
            colorPicker.setValue(prevStop.getColor());
            cir.setStop1(stop);
        }
    }
}
