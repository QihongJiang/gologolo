/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.BORDER_THICKNESS;
import gologolo.data.GoLogoLoCircle;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoRectangle;
import javafx.scene.control.Slider;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class BorderThickness_Transaction implements jTPS_Transaction{
    double borderThickness;
    double preValue;
    GoLogoLoApp app;
    GoLogoLoRectangle rec;
    GoLogoLoCircle cir;
    GoLogoLoData data;
    Shape shape;
    Slider slider;
    
    public BorderThickness_Transaction(GoLogoLoApp initApp, double value){
        app = initApp;
        borderThickness = value;
        
        data = (GoLogoLoData) app.getDataComponent();
        int index = data.getItems().indexOf(data.getSelectedItem());
        slider = (Slider) app.getGUIModule().getGUINode(BORDER_THICKNESS);
        
        shape = (Shape) data.getNode(index);
        if(data.getSelectedItem().isIsCircle()){
            cir = (GoLogoLoCircle) shape;
            preValue = cir.getBorderThickness();
        }
        else{
            rec = (GoLogoLoRectangle) shape;
            preValue = rec.getBorderThickness();
        }
        
        
        
    }
    
    @Override
    public void doTransaction() {
        slider.adjustValue(borderThickness);
        if(data.getSelectedItem().isIsCircle()){
            cir.setBorderThickness(borderThickness);
            cir.setStrokeWidth(borderThickness);
        }
        else{
            rec.setBorderThickness(borderThickness);
            rec.setStrokeWidth(borderThickness);
        }
       
    }

    @Override
    public void undoTransaction() {
        if(data.getSelectedItem().isIsCircle()){
            cir.setStrokeWidth(preValue);
            cir.setBorderThickness(preValue);
        }
        else{
            rec.setStrokeWidth(preValue);
            rec.setBorderThickness(preValue);
        }
        
        slider.adjustValue(preValue);
    }
    
}
