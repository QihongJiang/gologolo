/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoItemPrototype;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class EditItem_Transaction implements jTPS_Transaction{
    GoLogoLoData data;
    GoLogoLoItemPrototype itemToEdit;
    GoLogoLoItemPrototype dataTemp;
    
    public EditItem_Transaction(GoLogoLoData initData, GoLogoLoItemPrototype initNewItem){
        data = initData;
        itemToEdit = initNewItem;
    }

    @Override
    public void doTransaction() {
        dataTemp = (GoLogoLoItemPrototype) data.getSelectedItem().clone();
        data.getSelectedItem().setName(itemToEdit.getName());
    }

    @Override
    public void undoTransaction() {
        if(data.isItemSelected()){
            data.getSelectedItem().setType(dataTemp.getType());
        }
    }
    
}
