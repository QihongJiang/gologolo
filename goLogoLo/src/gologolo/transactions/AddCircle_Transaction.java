/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.data.GoLogoLoCircle;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoItemPrototype;
import gologolo.data.GoLogoLoRectangle;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class AddCircle_Transaction implements jTPS_Transaction{
    GoLogoLoData data;
    GoLogoLoItemPrototype itemToAdd;
    GoLogoLoCircle circ;
    
    public AddCircle_Transaction(GoLogoLoData initData, GoLogoLoItemPrototype initNewItem){
        itemToAdd = initNewItem;
        data=initData;
    }
    
    @Override
    public void doTransaction() {
        itemToAdd.setIsText(false);
        itemToAdd.setIsShape(false);
        itemToAdd.setIsCircle(true);itemToAdd.setIsImage(false);
        data.addItem(itemToAdd);
        circ = new GoLogoLoCircle(60, 30, 30);
        circ.setRadius(60);
        circ.setCenterX(100);
        circ.setCenterY(100);
        circ.setFill(Color.WHITE);
        circ.setStroke(Color.BLACK);
        data.addNode(circ);
    }

    @Override
    public void undoTransaction() {
        data.removeNode(circ);
        data.removeItem(itemToAdd);
    }
}
