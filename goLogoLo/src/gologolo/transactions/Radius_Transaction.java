/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.CENTER_X;
import static gologolo.GoLogoLoPropertyType.RADIUS;
import gologolo.data.GoLogoLoCircle;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoRectangle;
import javafx.scene.control.Slider;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class Radius_Transaction implements jTPS_Transaction{
    RadialGradient gradient;
    double radius;
    double prevRadius;
    GoLogoLoApp app;
    GoLogoLoData data;
    GoLogoLoCircle cir;
    Slider slider;
    GoLogoLoRectangle rec;
    public Radius_Transaction(GoLogoLoApp initApp, double value){
        app = initApp;
        radius = value;
        data = (GoLogoLoData) app.getDataComponent();
        if(data.getSelectedItem().isIsCircle()){
            cir = (GoLogoLoCircle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevRadius = cir.getRadialRadius();
        }
        else{
            rec = (GoLogoLoRectangle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevRadius = rec.getRadius();
        }
        slider = (Slider) app.getGUIModule().getGUINode(RADIUS);
    }
    
    @Override
    public void doTransaction() {
        
        if(data.getSelectedItem().getIsShape()){
            gradient = new RadialGradient(rec.getFocusAngle(), rec.getFocusDistance(), 
                                          rec.getCenterX(), rec.getCenterY(),
                                            radius, rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), rec.getStop1());
            rec.setRadius(radius);
            slider.setValue(radius);
            rec.setFill(gradient);
        }
        else{
            gradient = new RadialGradient(cir.getFocusAngle(), cir.getFocusDistance(), 
                                          cir.getRadialX(), cir.getRadialY(),
                                            radius, cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), cir.getStop1());
            cir.setRadius(radius);
            slider.setValue(radius);
            cir.setFill(gradient);
        }
    }

    @Override
    public void undoTransaction() {
        if(data.getSelectedItem().getIsShape()){
            gradient = new RadialGradient(rec.getFocusAngle(), rec.getFocusDistance(), 
                                          rec.getCenterX(), rec.getCenterY(),
                                            prevRadius, rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), rec.getStop1());
            rec.setRadius(radius);
            slider.setValue(prevRadius);
            rec.setFill(gradient);
        }
        else{
            gradient = new RadialGradient(cir.getFocusAngle(), cir.getFocusDistance(), 
                                          cir.getRadialX(), cir.getRadialY(),
                                            prevRadius, cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), cir.getStop1());
            cir.setRadialRadius(radius);
            slider.setValue(prevRadius);
            cir.setFill(gradient);
        }
    }
}
