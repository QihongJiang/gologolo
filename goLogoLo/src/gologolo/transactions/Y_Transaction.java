/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.CENTER_Y;
import gologolo.data.GoLogoLoCircle;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoRectangle;
import javafx.scene.control.Slider;
import javafx.scene.paint.RadialGradient;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class Y_Transaction implements jTPS_Transaction{
    RadialGradient gradient;
    double y;
    double prevY;
    GoLogoLoApp app;
    GoLogoLoRectangle rec;
    Slider slider;
    GoLogoLoData data;
    GoLogoLoCircle cir;
    public Y_Transaction(GoLogoLoApp initApp, double value){
        app = initApp;
        y = value;
        data = (GoLogoLoData) app.getDataComponent();
        if(data.getSelectedItem().isIsCircle()){
            cir = (GoLogoLoCircle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevY = cir.getRadialY();
        }
        else{
            rec = (GoLogoLoRectangle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevY = rec.getCenterY();
        }
        slider = (Slider) app.getGUIModule().getGUINode(CENTER_Y);
    }
    
    @Override
    public void doTransaction() {
        if(data.getSelectedItem().getIsShape()){
            gradient = new RadialGradient(rec.getFocusAngle(), rec.getFocusDistance(), 
                                          y, rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), rec.getStop1());
            rec.setCenterX(y);
            slider.adjustValue(y);
            rec.setFill(gradient);
        }
        else{
            gradient = new RadialGradient(cir.getFocusAngle(), cir.getFocusDistance(), 
                                          cir.getRadialX(), y,
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), cir.getStop1());
            cir.setRadialY(y);
            slider.adjustValue(y);
            cir.setFill(gradient);
        }
    }

    @Override
    public void undoTransaction() {
        
        if(data.getSelectedItem().getIsShape()){
            rec.setFill(new RadialGradient(rec.getFocusAngle(), rec.getFocusDistance(), 
                                          prevY, rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), rec.getStop1()));
        slider.adjustValue(prevY);
        rec.setCenterY(y);
        }
        else{
            cir.setFill(new RadialGradient(cir.getFocusAngle(), cir.getFocusDistance(), 
                                          prevY, cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), rec.getStop1()));
            slider.adjustValue(prevY);
            cir.setRadialY(y);
        }
    }
}
