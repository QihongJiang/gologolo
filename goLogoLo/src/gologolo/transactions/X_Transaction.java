/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.CENTER_X;
import gologolo.data.GoLogoLoCircle;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoRectangle;
import javafx.scene.control.Slider;
import javafx.scene.paint.RadialGradient;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class X_Transaction implements jTPS_Transaction{
    RadialGradient gradient;
    double x;
    double prevX;
    GoLogoLoApp app;
    GoLogoLoRectangle rec;
    Slider slider;
    GoLogoLoData data;
    GoLogoLoCircle cir;
    public X_Transaction(GoLogoLoApp initApp, double value){
        app = initApp;
        x = value;
        data = (GoLogoLoData) app.getDataComponent();
        if(data.getSelectedItem().isIsCircle()){
            cir = (GoLogoLoCircle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevX = cir.getRadialX();
        }
        else{
            rec = (GoLogoLoRectangle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevX = rec.getCenterX();
        }
        slider = (Slider) app.getGUIModule().getGUINode(CENTER_X);
    }
    
    @Override
    public void doTransaction() {
        if(data.getSelectedItem().getIsShape()){
            gradient = new RadialGradient(rec.getFocusAngle(), rec.getFocusDistance(), 
                                          x, rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), rec.getStop1());
            rec.setCenterX(x);
            slider.adjustValue(x);
            rec.setFill(gradient);
        }
        else{
            gradient = new RadialGradient(cir.getFocusAngle(), cir.getFocusDistance(), 
                                          x, cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), cir.getStop1());
            cir.setRadialX(x);
            slider.adjustValue(x);
            cir.setFill(gradient);
        }
    }

    @Override
    public void undoTransaction() {
        
        if(data.getSelectedItem().getIsShape()){
            rec.setFill(new RadialGradient(rec.getFocusAngle(), rec.getFocusDistance(), 
                                          prevX, rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), rec.getStop1()));
        slider.adjustValue(prevX);
        rec.setCenterX(x);
        }
        else{
            cir.setFill(new RadialGradient(cir.getFocusAngle(), cir.getFocusDistance(), 
                                          prevX, cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), rec.getStop1()));
            slider.adjustValue(prevX);
            cir.setRadialX(x);
        }
    }
    
}
