/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoCircle;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoRectangle;
import javafx.scene.paint.Color;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class BorderColor_Transaction implements jTPS_Transaction{
    RadialGradient gradient;
    Color color;
    Color prevColor;
    GoLogoLoApp app;
    GoLogoLoData data;
    GoLogoLoRectangle rec;
    GoLogoLoCircle cir;
    
    public BorderColor_Transaction(GoLogoLoApp initApp, Color value){
        app = initApp;
        color = value;
        data = (GoLogoLoData) app.getDataComponent();
        if(data.getSelectedItem().isIsCircle()){
            cir = (GoLogoLoCircle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevColor = cir.getBorderColor();
        }
        else{
            rec = (GoLogoLoRectangle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevColor = rec.getBorderColor();
        }
        
    }
    
    @Override
    public void doTransaction() {
        if(data.getSelectedItem().isIsCircle()){
            cir.setBorderColor(color);
            cir.setStroke(color);
        }
        else{
            rec.setBorderColor(color);
            rec.setStroke(color);
        }
            
    }

    @Override
    public void undoTransaction() {
        if(data.getSelectedItem().isIsCircle()){
            cir.setBorderColor(prevColor);
            cir.setStroke(prevColor);
        }
        else{
            rec.setBorderColor(prevColor);
           rec.setStroke(prevColor);
        }
    }
}
