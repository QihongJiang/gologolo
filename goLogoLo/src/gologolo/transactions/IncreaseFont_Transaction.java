/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoItemPrototype;
import gologolo.data.GoLogoLoText;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class IncreaseFont_Transaction implements jTPS_Transaction{
    GoLogoLoApp app;
    GoLogoLoItemPrototype selectedItem;
    GoLogoLoText textField;
    Font oldFont;
    Font font;
    double size;
    double oldSize;
    GoLogoLoData data;
    int index;
    
    public IncreaseFont_Transaction(GoLogoLoApp initApp, GoLogoLoText initText) {
        app = initApp;
        data = (GoLogoLoData) app.getDataComponent();
        textField = initText;
        size = textField.getSize() + 2;
        oldSize = textField.getSize();
    }

    @Override
    public void doTransaction() {
        if(data.getSelectedItem().getIsText()){
            if(textField.isBold()){
                oldFont = Font.font(textField.getFontToString(), FontWeight.BOLD, oldSize);
                font = Font.font(textField.getFontToString(), FontWeight.BOLD, size);
            }
            else if(textField.isItalic()){
                oldFont = Font.font(textField.getFontToString(), FontWeight.BOLD, oldSize);
                font = Font.font(textField.getFontToString(), FontPosture.ITALIC, size);
            }
            else{
                oldFont = new Font(oldSize);
                font = new Font(size);
            }
            textField.setFont(font);
            textField.setSize(size);
        }
    }

    @Override
    public void undoTransaction() {
        textField.setFont(oldFont);
        textField.setSize(oldSize);
    }
}
