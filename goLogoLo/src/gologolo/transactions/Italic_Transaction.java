/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoItemPrototype;
import gologolo.data.GoLogoLoText;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_TEXT_BOLD;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_TEXT_ITALIC;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;

import javafx.scene.text.FontWeight;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class Italic_Transaction implements jTPS_Transaction{
    GoLogoLoApp app;
    GoLogoLoItemPrototype selectedItem;
    GoLogoLoItemPrototype item;
    GoLogoLoData data;
    GoLogoLoText text;
    String font;
    Font tempFont;
    
    public Italic_Transaction(GoLogoLoApp initApp) {
        app = initApp;
        data = (GoLogoLoData)app.getDataComponent();
        item = data.getSelectedItem();
        text = (GoLogoLoText) data.getNode(data.getItems().indexOf(item));
        tempFont = text.getFont();
    }

    @Override
    public void doTransaction() {
        if(item.getIsText()){
            text.setItalic(true);
            text.setBold(false);
            text.setFont(Font.font(text.getFontToString(), FontPosture.ITALIC, text.getSize()));
        }
    }

    @Override
    public void undoTransaction() {
        text.setFont(Font.font(text.getFontToString(), FontPosture.REGULAR, text.getSize()));
    }
}
