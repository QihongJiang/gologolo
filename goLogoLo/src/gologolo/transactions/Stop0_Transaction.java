/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.STOP_0;
import static gologolo.GoLogoLoPropertyType.STOP_1;
import gologolo.data.GoLogoLoCircle;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoRectangle;
import javafx.scene.control.ColorPicker;
import javafx.scene.paint.Color;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class Stop0_Transaction implements jTPS_Transaction{
    RadialGradient gradient;
    Color color;
    Stop stop;
    Stop prevStop;
    GoLogoLoApp app;
    GoLogoLoData data;
    GoLogoLoRectangle rec;
    GoLogoLoCircle cir;
    ColorPicker colorPicker;
    Color prevColor;
    public Stop0_Transaction(GoLogoLoApp initApp, Color value){
        app = initApp;
        color = value;
        stop = new Stop(0, color);
        data = (GoLogoLoData) app.getDataComponent();
        if(data.getSelectedItem().getIsShape()){
            rec = (GoLogoLoRectangle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevStop = rec.getStop0();
        }
        else{
            cir = (GoLogoLoCircle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevStop = cir.getStop0();
        }
        colorPicker = (ColorPicker) app.getGUIModule().getGUINode(STOP_1);
        colorPicker = (ColorPicker) app.getGUIModule().getGUINode(STOP_0);
    }
    
    @Override
    public void doTransaction() {
        if(data.getSelectedItem().getIsShape()){
            
            gradient = new RadialGradient(rec.getFocusAngle(), rec.getFocusDistance(), 
                                          rec.getCenterX(), rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), stop, rec.getStop1());
            
            colorPicker.setValue(color);
            rec.setFill(gradient);
            rec.setStop0(stop);
        }
        else{
            gradient = new RadialGradient(cir.getFocusAngle(), cir.getFocusDistance(), 
                                          cir.getRadialX(), cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), stop, cir.getStop1());
            cir.setStop0(stop);
            colorPicker.setValue(stop.getColor());
            cir.setFill(gradient);
        }
    }

    @Override
    public void undoTransaction() {
        if(data.getSelectedItem().getIsShape()){
        rec.setFill(new RadialGradient(rec.getFocusAngle(), rec.getFocusDistance(), 
                                        rec.getCenterX(), rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), prevStop, rec.getStop1()));
        colorPicker.setValue(prevStop.getColor());
        rec.setStop0(stop);
        }
        else{
            gradient = new RadialGradient(cir.getFocusAngle(), cir.getFocusDistance(), 
                                          cir.getRadialX(), cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), prevStop, cir.getStop1());
            colorPicker.setValue(prevStop.getColor());
            cir.setStop0(stop);
        }
    }
    
}
