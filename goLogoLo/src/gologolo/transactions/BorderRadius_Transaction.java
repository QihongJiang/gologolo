/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoRectangle;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class BorderRadius_Transaction implements jTPS_Transaction{
    double borderRadius;
    double preValue;
    GoLogoLoApp app;
    GoLogoLoRectangle rec;
    
    public BorderRadius_Transaction(GoLogoLoApp initApp, double value){
        app = initApp;
        borderRadius = value;
        GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
        int index = data.getItems().indexOf(data.getSelectedItem());
        rec = (GoLogoLoRectangle) data.getNode(index);
        preValue = rec.getBorderThickness();
    }
    
    @Override
    public void doTransaction() {
        
        rec.setBorderRadius(borderRadius);
        rec.setArcHeight(borderRadius);
        rec.setArcWidth(borderRadius);
    }

    @Override
    public void undoTransaction() {
        rec.setBorderRadius(preValue);
        rec.setArcHeight(preValue);
        rec.setArcWidth(preValue);
    }
    
}
