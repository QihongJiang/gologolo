/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.transactions;

import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.FOCUS_ANGLE;
import static gologolo.GoLogoLoPropertyType.FOCUS_DISTANCE;
import gologolo.data.GoLogoLoCircle;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoRectangle;
import javafx.scene.control.Slider;
import javafx.scene.paint.RadialGradient;
import jtps.jTPS_Transaction;

/**
 *
 * @author qihong
 */
public class FocusDistance_Transaction implements jTPS_Transaction{
    RadialGradient gradient;
    GoLogoLoData data;
    double focusDistance;
    double prevFocusDistance;
    GoLogoLoApp app;
    GoLogoLoRectangle rec;
    GoLogoLoCircle cir;
    Slider slider;
    public FocusDistance_Transaction(GoLogoLoApp initApp, double value){
        app = initApp;
        focusDistance = value;
        data = (GoLogoLoData) app.getDataComponent();
        if(data.getSelectedItem().isIsCircle()){
            cir = (GoLogoLoCircle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevFocusDistance = cir.getFocusDistance();
        }
        else{
            rec = (GoLogoLoRectangle) data.getNode(data.getItemIndex(data.getSelectedItem()));
            prevFocusDistance = rec.getFocusDistance();
        }
        slider = (Slider) app.getGUIModule().getGUINode(FOCUS_DISTANCE);
    }
    
    @Override
    public void doTransaction() {
        if(data.getSelectedItem().getIsShape()){
            gradient = new RadialGradient(rec.getFocusAngle(), focusDistance, 
                                          rec.getCenterX(), rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), rec.getStop1());
            rec.setFocusDistance(focusDistance);
            slider.adjustValue(focusDistance);
            rec.setFill(gradient);
        }
        else{
            gradient = new RadialGradient(cir.getFocusAngle(), focusDistance,
                                          cir.getRadialX(), cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), cir.getStop1());
            cir.setFocusDistance(focusDistance);
            slider.adjustValue(focusDistance);
            cir.setFill(gradient);
        }
    }

    @Override
    public void undoTransaction() {
        if(data.getSelectedItem().getIsShape()){
            rec.setFill(new RadialGradient(rec.getFocusAngle(), prevFocusDistance, 
                                          rec.getCenterX(), rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), rec.getStop1()));
            rec.setFocusDistance(prevFocusDistance);
        }
        else{
            cir.setFill(new RadialGradient(cir.getFocusAngle(), prevFocusDistance,
                                          cir.getRadialX(), cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), cir.getStop1()));
            cir.setFocusDistance(prevFocusDistance);
        }
        slider.adjustValue(prevFocusDistance);
    }
    
}
