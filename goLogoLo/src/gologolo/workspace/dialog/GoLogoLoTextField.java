/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.dialog;

import djf.modules.AppLanguageModule;
import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_HEADER;;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_TXT_FIELD_HEADER_TEXT;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_TXT_PROMPT;
import static gologolo.GoLogoLoPropertyType.LOGO_DIALOG_CANCEL_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_DIALOG_OK_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_FIELD_CANCEL_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_FIELD_OK_BUTTON;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoItemPrototype;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_BUTTON;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_GRID;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_HEADER;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_PANE;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_PROMPT;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_TEXT_FIELD;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author qihong
 */
public class GoLogoLoTextField extends Stage{
    GoLogoLoApp app;
    GridPane gridPane;
    
    String text;
    TextField textField;
    
    GoLogoLoItemPrototype newItem;
    GoLogoLoItemPrototype editItem;
    
    Label headerLabel = new Label();    
    Label textLabel = new Label();
    TextField textTextField = new TextField();
    
    Button okButton = new Button();
    Button cancelButton = new Button();
    HBox okCancelPane = new HBox();
    
    EventHandler cancelHandler;
    EventHandler addItemOkHandler;
    EventHandler editItemOkHandler;
    
    GoLogoLoItemPrototype itemToEdit;
    public GoLogoLoTextField(GoLogoLoApp initApp) {
        // KEEP THIS FOR WHEN THE WORK IS ENTERED
        app = initApp;
        gridPane = new GridPane();
        gridPane.getStyleClass().add(CLASS_LOGO_DIALOG_GRID);
        initDialog();
        
        // NOW PUT THE GRID IN THE SCENE AND THE SCENE IN THE DIALOG
        Scene scene = new Scene(gridPane);
        this.setScene(scene);
        
        // SETUP THE STYLESHEET
        app.getGUIModule().initStylesheet(this);
        scene.getStylesheets().add(CLASS_LOGO_DIALOG_GRID);     
    }
    private void initDialog() {
        initGridNode(headerLabel,          LOGO_ITEM_DIALOG_HEADER,                CLASS_LOGO_DIALOG_HEADER,       0, 0, 3, 1, true);
        initGridNode(textLabel,            LOGO_ITEM_DIALOG_TXT_PROMPT,          CLASS_LOGO_DIALOG_PROMPT,       0, 1, 1, 1, true);
        initGridNode(textTextField,        null,          CLASS_LOGO_DIALOG_TEXT_FIELD,   1, 1, 1, 1, false);
        initGridNode(okCancelPane,          null,                                  CLASS_LOGO_DIALOG_PANE,         0, 2, 3, 1, false);
        
        okButton = new Button("OK");
        cancelButton = new Button("CANCEL");
        app.getGUIModule().addGUINode(LOGO_DIALOG_OK_BUTTON, okButton);
        app.getGUIModule().addGUINode(LOGO_DIALOG_CANCEL_BUTTON, cancelButton);
        okButton.getStyleClass().add(CLASS_LOGO_DIALOG_BUTTON);
        cancelButton.getStyleClass().add(CLASS_LOGO_DIALOG_BUTTON);
        okCancelPane.getChildren().add(okButton);
        okCancelPane.getChildren().add(cancelButton);
        okCancelPane.setAlignment(Pos.CENTER);
        
        textTextField.setOnAction(e->{
            processCompleteWork();
        });
        okButton.setOnAction(e->{
            processCompleteWork();
        });
        cancelButton.setOnAction(e->{
            newItem = null;
            editItem = null;
            this.hide();
        });   
    }
    
    public void showEditDialog() {
        
        // USE THE TEXT IN THE HEADER FOR EDIT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String headerText = props.getProperty(LOGO_ITEM_DIALOG_TXT_FIELD_HEADER_TEXT);
        headerLabel.setText(headerText);
        setTitle(headerText);
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        // WE'LL ONLY PROCEED IF THERE IS A LINE TO EDIT
        
        // AND OPEN THE DIALOG
        showAndWait();
        
    }
    
    private void processCompleteWork() {
        // GET THE SETTINGS
        text = textTextField.getText();
        textField = textTextField;
        // CLOSE THE DIALOG
        this.hide();
    }
    
    protected void initGridNode(Node node, Object nodeId, String styleClass, int col, int row, int colSpan, int rowSpan, boolean isLanguageDependent) {
        // GET THE LANGUAGE SETTINGS
        AppLanguageModule languageSettings = app.getLanguageModule();
        
        // TAKE CARE OF THE TEXT
        if (isLanguageDependent) {
            languageSettings.addLabeledControlProperty(nodeId + "_TEXT", ((Labeled)node).textProperty());
            ((Labeled)node).setTooltip(new Tooltip(""));
            languageSettings.addLabeledControlProperty(nodeId + "_TOOLTIP", ((Labeled)node).tooltipProperty().get().textProperty());
        }
        
        // PUT IT IN THE UI
        if (col >= 0)
            gridPane.add(node, col, row, colSpan, rowSpan);

        // SETUP IT'S STYLE SHEET
        node.getStyleClass().add(styleClass);
    }
    
    public TextField getTextField(){
        return this.textField;
    }
    
    public String getText(){
        return this.text;
    }
    
    public void setText(){
        this.textField.replaceText(0, textField.getLength(), text);
    }
}
