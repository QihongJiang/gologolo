/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.dialog;

import djf.modules.AppLanguageModule;
import gologolo.GoLogoLoApp;
import java.time.LocalDate;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import properties_manager.PropertiesManager;
/**
 *
 * @author Qihong
 */
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_ADD_HEADER_TEXT;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_CANCEL_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_EDIT_HEADER_TEXT;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_HEADER;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_HEIGHT_PROMPT;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_NAME_PROMPT;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_OK_BUTTON;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_ORDER_PROMPT;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_TXTE_PROMPT;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_TXT_HEADER;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_TXT_PROMPT;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_TYPE_PROMPT;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEM_DIALOG_WIDTH_PROMPT;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoItemPrototype;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_BUTTON;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_GRID;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_HEADER;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_PANE;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_PROMPT;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_TEXT_FIELD;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
public class GoLogoLoDialog extends Stage{
    GoLogoLoApp app;
    GridPane gridPane;
    boolean editing;
    boolean editText;
    String text;
    
    GoLogoLoItemPrototype newItem;
    GoLogoLoItemPrototype editItem;
    
    Label headerLabel = new Label();    
    Label categoryLabel = new Label();
    Label orderLabel = new Label();
    TextField orderTextField = new TextField();
    Label nameLabel = new Label();
    TextField nameTextField = new TextField();
    Label typeLabel = new Label();
    TextField typeTextField = new TextField();
    Label textLabel = new Label();
    TextField textTextField = new TextField();
    
    Button okButton = new Button();
    Button cancelButton = new Button();
    HBox okCancelPane = new HBox();
    
    EventHandler cancelHandler;
    EventHandler addItemOkHandler;
    EventHandler editItemOkHandler;
    
    GoLogoLoItemPrototype itemToEdit;
    public GoLogoLoDialog(GoLogoLoApp initApp) {
        // KEEP THIS FOR WHEN THE WORK IS ENTERED
        app = initApp;
        gridPane = new GridPane();
        gridPane.getStyleClass().add(CLASS_LOGO_DIALOG_GRID);
        initDialog();
        
        // NOW PUT THE GRID IN THE SCENE AND THE SCENE IN THE DIALOG
        Scene scene = new Scene(gridPane);
        this.setScene(scene);
        
        // SETUP THE STYLESHEET
        app.getGUIModule().initStylesheet(this);
        scene.getStylesheets().add(CLASS_LOGO_DIALOG_GRID);     
    }
    
    public void showAddDialog() {        
        // USE THE TEXT IN THE HEADER FOR ADD
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String headerText = props.getProperty(LOGO_ITEM_DIALOG_ADD_HEADER_TEXT);
        
        setTitle(headerText);
        
        // USE THE TEXT IN THE HEADER FOR ADD
        
        
        // WE ARE ADDING A NEW ONE, NOT EDITING
        editing = false;
        editText = false;
        
        // AND OPEN THE DIALOG
        showAndWait();
    }
    
    public void showEditDialog(GoLogoLoItemPrototype initItemToEdit) {
        // WE'LL NEED THIS FOR VALIDATION
        itemToEdit = initItemToEdit;
        
        // USE THE TEXT IN THE HEADER FOR EDIT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String headerText = props.getProperty(LOGO_ITEM_DIALOG_EDIT_HEADER_TEXT);
        headerLabel.setText(headerText);
        setTitle(headerText);
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        // WE'LL ONLY PROCEED IF THERE IS A LINE TO EDIT
        editing = true;
        editItem = data.getSelectedItem();
        if(this.getEditItem()==null){
            editing = false;
        }
        if(editing){
            nameTextField.setText(editItem.getName());
            typeTextField.setText(editItem.getType());
        }
        // AND OPEN THE DIALOG
        showAndWait();
        
    }
    
    public void showEditTextDialog(){
        editText = true;
        // USE THE TEXT IN THE HEADER FOR EDIT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String headerText = props.getProperty(LOGO_ITEM_DIALOG_EDIT_HEADER_TEXT);
        headerLabel.setText(headerText);
        setTitle(headerText);
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        text = textTextField.getPromptText();
        // AND OPEN THE DIALOG
        showAndWait();
        editText = false;
    }
    
    private void initDialog() {
        initGridNode(headerLabel,          LOGO_ITEM_DIALOG_TXT_HEADER,                CLASS_LOGO_DIALOG_HEADER,       0, 0, 3, 1, true);
        initGridNode(nameLabel,            LOGO_ITEM_DIALOG_NAME_PROMPT,           CLASS_LOGO_DIALOG_PROMPT,       0, 1, 1, 1, true);
        initGridNode(nameTextField,        null,           CLASS_LOGO_DIALOG_TEXT_FIELD,   1, 1, 1, 1, false);
        initGridNode(textLabel,            LOGO_ITEM_DIALOG_TXTE_PROMPT,          CLASS_LOGO_DIALOG_PROMPT,       0, 2, 1, 1, true);
        initGridNode(textTextField,        null,                                   CLASS_LOGO_DIALOG_TEXT_FIELD,   1, 2, 1, 1, false);
        initGridNode(okCancelPane,          null,                                  CLASS_LOGO_DIALOG_PANE,         0, 3, 3, 1, false);
        okButton = new Button();
        cancelButton = new Button();
        app.getGUIModule().addGUINode(LOGO_ITEM_DIALOG_OK_BUTTON, okButton);
        app.getGUIModule().addGUINode(LOGO_ITEM_DIALOG_CANCEL_BUTTON, cancelButton);
        okButton.getStyleClass().add(CLASS_LOGO_DIALOG_BUTTON);
        cancelButton.getStyleClass().add(CLASS_LOGO_DIALOG_BUTTON);
        okCancelPane.getChildren().add(okButton);
        okCancelPane.getChildren().add(cancelButton);
        okCancelPane.setAlignment(Pos.CENTER);
        
        AppLanguageModule languageSettings = app.getLanguageModule();
        languageSettings.addLabeledControlProperty(LOGO_ITEM_DIALOG_OK_BUTTON + "_TEXT",    okButton.textProperty());
        languageSettings.addLabeledControlProperty(LOGO_ITEM_DIALOG_CANCEL_BUTTON + "_TEXT",    cancelButton.textProperty());
        
        nameTextField.setOnAction(e->{
            processCompleteWork();
        });
        textTextField.setOnAction(e->{
            processCompleteWork();
        });
        okButton.setOnAction(e->{
            processCompleteWork();
        });
        cancelButton.setOnAction(e->{
            newItem = null;
            editItem = null;
            this.hide();
        });   
    }
    
    protected void initGridNode(Node node, Object nodeId, String styleClass, int col, int row, int colSpan, int rowSpan, boolean isLanguageDependent) {
        // GET THE LANGUAGE SETTINGS
        AppLanguageModule languageSettings = app.getLanguageModule();
        
        // TAKE CARE OF THE TEXT
        if (isLanguageDependent) {
            languageSettings.addLabeledControlProperty(nodeId + "_TEXT", ((Labeled)node).textProperty());
            ((Labeled)node).setTooltip(new Tooltip(""));
            languageSettings.addLabeledControlProperty(nodeId + "_TOOLTIP", ((Labeled)node).tooltipProperty().get().textProperty());
        }
        
        // PUT IT IN THE UI
        if (col >= 0)
            gridPane.add(node, col, row, colSpan, rowSpan);

        // SETUP IT'S STYLE SHEET
        node.getStyleClass().add(styleClass);
    }
    
    private void processCompleteWork() {
        // GET THE SETTINGS
        int order = 0;
        String type = "";
        String name = nameTextField.getText();
        this.text = textTextField.getText();
        
        // IF WE ARE EDITING
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if (editing) {
            if (data.isValidEditItem(itemToEdit,order, name)) {
                editItem = new GoLogoLoItemPrototype(order, name, type);
            }
            else{
                if(name.equals("")){
                    editItem = new GoLogoLoItemPrototype(order, name, type);
                }
            }
        }
        // IF WE ARE ADDING
        else {
            if (data.isValidNewGoLogoLoItem(order, name)) {
                    this.makeNewItem();
                
            }
            else {
                //if(category.equals("")||description.equals("")||startDate.equals("")||endDate.equals("")||assignedTo.equals("")){
                    newItem = new GoLogoLoItemPrototype(order, name, type);
            }
        }
        // CLOSE THE DIALOG
        this.hide();
    }
    
    private void makeNewItem() {
        int order = 0;
        String name = nameTextField.getText();
        String type = typeTextField.getText();
        newItem = new GoLogoLoItemPrototype(order, name, type);
        this.hide();
    }
    
    public GoLogoLoItemPrototype getNewItem() {
        return newItem;
    }
    
    public GoLogoLoItemPrototype getEditItem() {
        return editItem;
    }

    public String getText() {
        return this.text;
    }
}
