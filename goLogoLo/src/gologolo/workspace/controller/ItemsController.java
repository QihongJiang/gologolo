/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.controller;
import djf.AppPropertyType;
import static djf.AppPropertyType.RESIZE_HEADER_TEXT;
import java.util.ArrayList;
import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoData;
import djf.ui.dialogs.AppDialogsFacade;
import djf.ui.dialogs.AppResizeDialog;
import gologolo.data.GoLogoLoItemPrototype;
import gologolo.data.GoLogoLoText;
import gologolo.transactions.AddCircle_Transaction;
import gologolo.transactions.AddImage_Transaction;
import gologolo.transactions.AddRectangle_Transaction;
import gologolo.transactions.AddText_Transaction;
import gologolo.transactions.Bold_Transaction;
import gologolo.transactions.BorderColor_Transaction;
import gologolo.transactions.BorderRadius_Transaction;
import gologolo.transactions.BorderThickness_Transaction;
import gologolo.transactions.ChangeFont_Transaction;
import gologolo.transactions.ChangeSize_Transaction;
import gologolo.transactions.Cycle_Transaction;
import gologolo.transactions.DecreaseFont_Transaction;
import gologolo.transactions.EditItem_Transaction;
import gologolo.transactions.EditTextField_Transaction;
import gologolo.transactions.FocusAngle_Transaction;
import gologolo.transactions.FocusDistance_Transaction;
import gologolo.transactions.IncreaseFont_Transaction;
import gologolo.transactions.Italic_Transaction;
import gologolo.transactions.MoveDown_Transaction;
import gologolo.transactions.MoveUp_Transaction;
import gologolo.transactions.Radius_Transaction;
import gologolo.transactions.RemoveItem_Transaction;
import gologolo.transactions.Resize_Transaction;
import gologolo.transactions.Stop0_Transaction;
import gologolo.transactions.Stop1_Transaction;
import gologolo.transactions.UnderLine_Transaction;
import gologolo.transactions.X_Transaction;
import gologolo.transactions.Y_Transaction;
import gologolo.workspace.dialog.GoLogoLoDialog;
import gologolo.workspace.dialog.GoLogoLoRenameDialog;
import gologolo.workspace.dialog.GoLogoLoTextField;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Stack;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.text.Text;
/**
 *
 * @author Qihong
 */
public class ItemsController {
    
    GoLogoLoApp app;
    GoLogoLoDialog itemDialog;
    GoLogoLoTextField logoText;
    Stack deletedOrder;
    
    static int count = 0;
    int index;
    Pane pane;
    public ItemsController(GoLogoLoApp initApp) {
        app = initApp;
        logoText = new GoLogoLoTextField(app);
        itemDialog = new GoLogoLoDialog(app);
        deletedOrder = new Stack();
    }
    
    public void processAddText() {
        itemDialog.showAddDialog();
        GoLogoLoItemPrototype newItem = itemDialog.getNewItem();    
        newItem.setType("Text");
        if (newItem != null) {
            // IF IT HAS A UNIQUE NAME AND COLOR
            // THEN CREATE A TRANSACTION FOR IT
            // AND ADD IT
            GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
            if(deletedOrder.isEmpty()){
                newItem.setOrder(++count);
            }
            else{
                newItem.setOrder((int)deletedOrder.pop());
            }
            AddText_Transaction transaction = new AddText_Transaction(data, newItem, itemDialog);
            app.processTransaction(transaction);
            
        }    
        
        // OTHERWISE TELL THE USER WHAT THEY
        // HAVE DONE WRONG
        else {
           djf.ui.dialogs.AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), "INVALID_LINE", "Invalid data for a new line");
        }
    }
    
    public void processAddImage() throws FileNotFoundException{
        int tempCount = 0;
        if(deletedOrder.isEmpty()){
                count++;
                tempCount = count;
        }
        else{
            tempCount = (int)deletedOrder.pop();
        }
        GoLogoLoItemPrototype newItem = new GoLogoLoItemPrototype(tempCount, "ANY", "image");
        File file = djf.ui.dialogs.AppDialogsFacade.showOpenDialog(itemDialog, AppPropertyType.APP_TITLE).getAbsoluteFile();
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        AddImage_Transaction transaction = new AddImage_Transaction(data, file, newItem);
        app.processTransaction(transaction);
    }
    
    public void processAddRectangle() {
        int tempCount = 0;
        if(deletedOrder.isEmpty()){
                count++;
                tempCount = count;
        }
        else{
            tempCount = (int)deletedOrder.pop();
        }
        GoLogoLoItemPrototype newItem = new GoLogoLoItemPrototype(tempCount, "ANY", "rectangle");
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        AddRectangle_Transaction transaction = new AddRectangle_Transaction(data, newItem);
        app.processTransaction(transaction);   
    }
    
    public void processAddCircle(){
        int tempCount = 0;
        if(deletedOrder.isEmpty()){
                count++;
                tempCount = count;
        }
        else{
            tempCount = (int)deletedOrder.pop();
        }
        GoLogoLoItemPrototype newItem = new GoLogoLoItemPrototype(tempCount, "ANY", "circle");
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        AddCircle_Transaction transaction = new AddCircle_Transaction(data, newItem);
        app.processTransaction(transaction);   
    }
    
    public void processEditItems(){
        itemDialog.showEditDialog(itemDialog.getNewItem());
        GoLogoLoItemPrototype newItem = itemDialog.getEditItem();
        
        if (newItem != null) {
            // IF IT HAS A UNIQUE NAME AND COLOR
            // THEN CREATE A TRANSACTION FOR IT
            // AND ADD IT
            GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
            EditItem_Transaction transaction = new EditItem_Transaction(data, newItem);
            app.processTransaction(transaction);
            
        }
    }
    
    public void processEditText(){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if(data.getSelectedItem().getIsText()){
            logoText.showEditDialog();
        
            EditTextField_Transaction transaction = new EditTextField_Transaction(data, logoText, count);
            app.processTransaction(transaction);
        }
    }
    
    public void processChangeFont(String font){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if (data.isItemSelected() || data.areItemsSelected()) {
            ChangeFont_Transaction transaction = new ChangeFont_Transaction(data, font);
            app.processTransaction(transaction);
        }
    }
    
    public void processChangeSize(String size){
         GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        index = data.getItems().indexOf(data.getSelectedItem());
        GoLogoLoText textField = (GoLogoLoText) data.getNode(index);
        if (data.isItemSelected() || data.areItemsSelected()) {
            double value = Double.parseDouble(size);
            ChangeSize_Transaction transaction = new ChangeSize_Transaction(data, value, textField);
            app.processTransaction(transaction);
        }
    }
    
    public void processRemoveItems() {
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if (data.isItemSelected() || data.areItemsSelected()) {
            ArrayList<GoLogoLoItemPrototype> itemsToRemove = new ArrayList(data.getSelectedItems());
            index = data.getItemIndex(data.getSelectedItem());
            deletedOrder.push(data.getSelectedItem().getOrder());
            RemoveItem_Transaction transaction = new RemoveItem_Transaction(app, itemsToRemove, count);
            app.processTransaction(transaction);
        }
    }
    
    public void processMoveUp(){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if(data.isItemSelected() || data.areItemsSelected()){
            MoveUp_Transaction transaction = new MoveUp_Transaction(app);
            app.processTransaction(transaction);
        }
    }
    
    public void processMoveDown(){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if(data.isItemSelected() || data.areItemsSelected()){
            MoveDown_Transaction transaction = new MoveDown_Transaction(app);
            app.processTransaction(transaction);
        }
    }
    
    public void processIncreaseFont(){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        index = data.getItems().indexOf(data.getSelectedItem());
        GoLogoLoText textField = (GoLogoLoText) data.getNode(index);
        if(data.isItemSelected() || data.areItemsSelected()){
            IncreaseFont_Transaction transaction = new IncreaseFont_Transaction(app, textField);
            app.processTransaction(transaction);
        }
    }
    
    public void processDecreaseFont(){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        index = data.getItems().indexOf(data.getSelectedItem());
        GoLogoLoText textField = (GoLogoLoText) data.getNode(index);
        if(data.isItemSelected() || data.areItemsSelected()){
            DecreaseFont_Transaction transaction = new DecreaseFont_Transaction(app, textField);
            app.processTransaction(transaction);
        }
    }
    
    public void processItalic(){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if(data.isItemSelected() || data.areItemsSelected()){
            Italic_Transaction transaction = new Italic_Transaction(app);
            app.processTransaction(transaction);
        }
    }
    
    public void processBold(){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if(data.isItemSelected() || data.areItemsSelected()){
            Bold_Transaction transaction = new Bold_Transaction(app);
            app.processTransaction(transaction);
        }
    }
    
    public void processUnderLine(Color color){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if(data.isItemSelected() || data.areItemsSelected()){
            UnderLine_Transaction transaction = new UnderLine_Transaction(app, color);
            app.processTransaction(transaction);
        }
    }
    
    public void processResize(){
        double[]tempArray = djf.ui.dialogs.AppDialogsFacade.showResizeDialog(app.getGUIModule().getWindow(), RESIZE_HEADER_TEXT);
        double height = tempArray[0];
        double width = tempArray[1];
        
        Resize_Transaction transaction = new Resize_Transaction(app, height, width);
        app.processTransaction(transaction);
    }
    
    public void processBorderThickness(double value){
        GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
        if(data.isItemSelected()|| data.areItemsSelected()){
            BorderThickness_Transaction transaction = new BorderThickness_Transaction(app, value);
            app.processTransaction(transaction);
        }
    }
    
    public void processBorderRadius(double value){
        GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
        if(data.isItemSelected()|| data.areItemsSelected()){
            BorderRadius_Transaction transaction = new BorderRadius_Transaction(app, value);
            app.processTransaction(transaction);
        }
    }
    
    public void processFocusAngle(double value){
        GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
        if(data.isItemSelected()|| data.areItemsSelected()){
            FocusAngle_Transaction transaction = new FocusAngle_Transaction(app, value);
            app.processTransaction(transaction);
        }
    }
    
    public void processFocusDistance(double value){
        GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
        if(data.isItemSelected()|| data.areItemsSelected()){
            FocusDistance_Transaction transaction = new FocusDistance_Transaction(app, value);
            app.processTransaction(transaction);
        }
    }
    
    public void processX(double value){
        GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
        if(data.isItemSelected()|| data.areItemsSelected()){
            X_Transaction transaction = new X_Transaction(app, value);
            app.processTransaction(transaction);
        }
    }
    
    public void processY(double value){
        GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
        if(data.isItemSelected()|| data.areItemsSelected()){
            Y_Transaction transaction = new Y_Transaction(app, value);
            app.processTransaction(transaction);
        }
    }
    
    public void processRadius(double value){
        GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
        if(data.isItemSelected()|| data.areItemsSelected()){
            Radius_Transaction transaction = new Radius_Transaction(app, value);
            app.processTransaction(transaction);
        }
    }
    
    public void processStop0(Color color){
        GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
        if(data.isItemSelected()|| data.areItemsSelected()){
            Stop0_Transaction transaction = new Stop0_Transaction(app, color);
            app.processTransaction(transaction);
        }
    }
    
    public void processStop1(Color color){
        GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
        if(data.isItemSelected()|| data.areItemsSelected()){
            Stop1_Transaction transaction = new Stop1_Transaction(app, color);
            app.processTransaction(transaction);
        }
    }
    
    public void processBorderColor(Color color){
        GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
        if(data.isItemSelected()|| data.areItemsSelected()){
            BorderColor_Transaction transaction = new BorderColor_Transaction(app, color);
            app.processTransaction(transaction);
        }
    }
    
    public void processCycleMethod(String input){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if (data.isItemSelected() || data.areItemsSelected()) {
            Cycle_Transaction transaction = new Cycle_Transaction(app, input);
            app.processTransaction(transaction);
        }
    }
    
    
}
