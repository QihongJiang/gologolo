package gologolo.workspace.controller;

import djf.AppTemplate;
import djf.modules.AppGUIModule;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.LOGO_ORDER_COLUMN;
import gologolo.data.GoLogoLoItemPrototype;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEMS_TABLE;

/**
 *
 * @author McKillaGorilla
 */
public class ItemsTableController {
    GoLogoLoApp app;

    public ItemsTableController(AppTemplate initApp) {
        app = (GoLogoLoApp)initApp;
    }

    public void processChangeTableSize() {
        AppGUIModule gui = app.getGUIModule();
        TableView<GoLogoLoItemPrototype> itemsTable = (TableView)gui.getGUINode(LOGO_ITEMS_TABLE);
        ObservableList columns = itemsTable.getColumns();
        for (int i = 0; i < columns.size(); i++) {
            TableColumn column = (TableColumn)columns.get(i);
            column.setMinWidth(itemsTable.widthProperty().getValue()/columns.size());
            column.setMaxWidth(itemsTable.widthProperty().getValue()/columns.size());
        }
    }    
}
