package gologolo.workspace.foolproof;

import djf.modules.AppGUIModule;
import djf.ui.foolproof.FoolproofDesign;
import javafx.scene.control.TextField;
import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.*;
import gologolo.data.GoLogoLoData;

/**
 *
 * @author McKillaGorilla
 */
public class LogoSelectionFoolProofDesign implements FoolproofDesign {
    GoLogoLoApp app;
    
    public LogoSelectionFoolProofDesign(GoLogoLoApp initApp) {
        app = initApp;
    }

    @Override
    public void updateControls() {
        AppGUIModule gui = app.getGUIModule();
        // CHECK AND SEE IF A TABLE ITEM IS SELECTED
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        boolean itemIsSelected = data.isItemSelected();
        boolean itemsAreSelected = data.areItemsSelected();
        
        gui.getGUINode(DELETE_COLUMN_BUTTON).setDisable(!(itemIsSelected || itemsAreSelected));
        gui.getGUINode(EDIT_ITEM_BUTTON).setDisable(!(itemIsSelected || itemsAreSelected));
        gui.getGUINode(MOVE_UP_BUTTON).setDisable(!(itemIsSelected || itemsAreSelected)||data.getItemIndex(data.getSelectedItem())==0||data.getNumItems()==0);
        gui.getGUINode(MOVE_DOWN_BUTTON).setDisable(!(itemIsSelected || itemsAreSelected)||data.getItemIndex(data.getSelectedItem())==data.getNumItems()-1);
        
        if(itemIsSelected||itemsAreSelected){
            gui.getGUINode(BORDER_THICKNESS).setDisable(!(data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape())||data.getSelectedItem().isIsImage());
            gui.getGUINode(BORDER_RADIUS).setDisable(!(data.getSelectedItem().getIsShape())||data.getSelectedItem().isIsImage());
            gui.getGUINode(BORDER_COLOR).setDisable(!(data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape())||data.getSelectedItem().isIsImage());
            gui.getGUINode(FOCUS_ANGLE).setDisable(!(data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape())||data.getSelectedItem().isIsImage());
            gui.getGUINode(FOCUS_DISTANCE).setDisable(!(data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape())||data.getSelectedItem().isIsImage());
            gui.getGUINode(CENTER_X).setDisable(!(data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape())||data.getSelectedItem().isIsImage());
            gui.getGUINode(CENTER_Y).setDisable(!(data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape())||data.getSelectedItem().isIsImage());
            gui.getGUINode(STOP_0).setDisable(!(data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape())||data.getSelectedItem().isIsImage());
            gui.getGUINode(STOP_1).setDisable(!(data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape())||data.getSelectedItem().isIsImage());
            gui.getGUINode(CYCLE_METHOD).setDisable(!(data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape())||data.getSelectedItem().isIsImage());
            gui.getGUINode(RADIUS).setDisable(!(data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape())||data.getSelectedItem().isIsImage());
        
            gui.getGUINode(FONT_DIALOG).setDisable((data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape()));
            gui.getGUINode(SIZE_DIALOG).setDisable((data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape()));
            gui.getGUINode(BOLD_BUTTON).setDisable((data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape()));
            gui.getGUINode(ITALICIZE_BUTTON).setDisable((data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape()));
        
            gui.getGUINode(INCREASE_TEXT_BUTTON).setDisable((data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape()));
            gui.getGUINode(DECREASE_TEXT_BUTTON).setDisable((data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape()));
            gui.getGUINode(UNDERLINE_BUTTON).setDisable((data.getSelectedItem().isIsCircle()||data.getSelectedItem().getIsShape()));
        }
        else{
            gui.getGUINode(BORDER_THICKNESS).setDisable(!(itemIsSelected || itemsAreSelected));
            gui.getGUINode(BORDER_RADIUS).setDisable(!(itemIsSelected || itemsAreSelected));
            gui.getGUINode(BORDER_COLOR).setDisable(!(itemIsSelected || itemsAreSelected));
            gui.getGUINode(FOCUS_ANGLE).setDisable(!(itemIsSelected || itemsAreSelected));
            gui.getGUINode(FOCUS_DISTANCE).setDisable(!(itemIsSelected || itemsAreSelected));
            gui.getGUINode(CENTER_X).setDisable(!(itemIsSelected || itemsAreSelected));
            gui.getGUINode(CENTER_Y).setDisable(!(itemIsSelected || itemsAreSelected));
            gui.getGUINode(STOP_0).setDisable(!(itemIsSelected || itemsAreSelected));
            gui.getGUINode(STOP_1).setDisable(!(itemIsSelected || itemsAreSelected));
            gui.getGUINode(CYCLE_METHOD).setDisable(!(itemIsSelected || itemsAreSelected));
            gui.getGUINode(RADIUS).setDisable(!(itemIsSelected || itemsAreSelected));
        
            gui.getGUINode(FONT_DIALOG).setDisable(!(itemIsSelected || itemsAreSelected));
            gui.getGUINode(SIZE_DIALOG).setDisable(!(itemIsSelected || itemsAreSelected));
            gui.getGUINode(BOLD_BUTTON).setDisable(!(itemIsSelected || itemsAreSelected));
            gui.getGUINode(ITALICIZE_BUTTON).setDisable(!(itemIsSelected || itemsAreSelected));
        
            gui.getGUINode(INCREASE_TEXT_BUTTON).setDisable(!(itemIsSelected || itemsAreSelected));
            gui.getGUINode(DECREASE_TEXT_BUTTON).setDisable(!(itemIsSelected || itemsAreSelected));
            gui.getGUINode(UNDERLINE_BUTTON).setDisable(!(itemIsSelected || itemsAreSelected));
        }
    }
}