/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace;
import static com.sun.webkit.CursorManager.ZOOM_IN;
import static com.sun.webkit.CursorManager.ZOOM_OUT;
import static djf.AppPropertyType.EXPAND_BUTTON;
import static djf.AppPropertyType.HAS_HOME;
import static djf.AppPropertyType.HOME_BUTTON;
import static djf.AppPropertyType.LOGO_RESIZE_DIALOG_OK_BUTTON;
import static djf.AppPropertyType.LOGO_RESIZE_DIALOG_OK_BUTTON_TEXT;
import static djf.AppPropertyType.RESIZE_HEADER_TEXT;
import static djf.AppPropertyType.TOP_TOOLBAR;
import static djf.AppPropertyType.ZOOM_IN_BUTTON;
import static djf.AppPropertyType.ZOOM_OUT_BUTTON;
import gologolo.workspace.controller.ItemsTableController;
import djf.components.AppWorkspaceComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import static djf.modules.AppGUIModule.DISABLED;
import static djf.modules.AppGUIModule.ENABLED;
import static djf.modules.AppGUIModule.FOCUS_TRAVERSABLE;
import static djf.modules.AppGUIModule.HAS_KEY_HANDLER;
import static djf.modules.AppGUIModule.NO_KEY_HANDLER;
import djf.ui.AppNodesBuilder;
import java.time.LocalDate;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SortEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.SortType;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.FlowPane;
import djf.components.AppWorkspaceComponent;
import static djf.ui.style.DJFStyle.CLASS_DJF_ICON_BUTTON;
import gologolo.data.GoLogoLoItemPrototype;
import gologolo.GoLogoLoApp;
import properties_manager.PropertiesManager;

import static gologolo.GoLogoLoPropertyType.LOGO_FOOLPROOF_SETTINGS;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_BOX;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_BOX_X;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_BOX_Y;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_BOX_Z;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_BIG_HEADER;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_BUTTON;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_COLUMN;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_PROMPT;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_TABLE;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_PANE;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_TOOL;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_TEXT_FIELD;
import static gologolo.GoLogoLoPropertyType.LOGO_PANE;
import static gologolo.GoLogoLoPropertyType.LOGO_LABEL;
import static gologolo.GoLogoLoPropertyType.LOGO_ADD_TEXT_BUTTON;
import static gologolo.GoLogoLoPropertyType.MOVE_UP_BUTTON;
import static gologolo.GoLogoLoPropertyType.MOVE_DOWN_BUTTON;
import static gologolo.GoLogoLoPropertyType.ADD_CIRCLE_BUTTON;
import static gologolo.GoLogoLoPropertyType.ADD_IMAGE_BUTTON;
import static gologolo.GoLogoLoPropertyType.ADD_RECTANGLE_BUTTON;
import static gologolo.GoLogoLoPropertyType.ADD_TRIANGLE_BUTTON;
import static gologolo.GoLogoLoPropertyType.BOLD_BUTTON;
import static gologolo.GoLogoLoPropertyType.BORDER_COLOR;
import static gologolo.GoLogoLoPropertyType.BORDER_RADIUS;
import static gologolo.GoLogoLoPropertyType.BORDER_THICKNESS;
import static gologolo.GoLogoLoPropertyType.CENTER_X;
import static gologolo.GoLogoLoPropertyType.CENTER_Y;
import static gologolo.GoLogoLoPropertyType.CYCLE_METHOD;
import static gologolo.GoLogoLoPropertyType.DECREASE_TEXT_BUTTON;
import static gologolo.GoLogoLoPropertyType.DEFAULT_FONT;
import static gologolo.GoLogoLoPropertyType.DEFAULT_SIZES;
import static gologolo.GoLogoLoPropertyType.DELETE_COLUMN_BUTTON;
import static gologolo.GoLogoLoPropertyType.INCREASE_TEXT_BUTTON;
import static gologolo.GoLogoLoPropertyType.ITALICIZE_BUTTON;
import static gologolo.GoLogoLoPropertyType.UNDERLINE_BUTTON;
import static gologolo.GoLogoLoPropertyType.EDIT_ITEM_BUTTON;
import static gologolo.GoLogoLoPropertyType.FOCUS_ANGLE;
import static gologolo.GoLogoLoPropertyType.FOCUS_DISTANCE;
import static gologolo.GoLogoLoPropertyType.FONT_DIALOG;
import static gologolo.GoLogoLoPropertyType.FONT_OPTIONS;
import static gologolo.GoLogoLoPropertyType.LOGO_NAME_COLUMN;
import static gologolo.GoLogoLoPropertyType.LOGO_ORDER_COLUMN;
import static gologolo.GoLogoLoPropertyType.LOGO_TYPE_COLUMN;
import static gologolo.GoLogoLoPropertyType.TDLM_ITEMS_TABLE_VIEW;
import static gologolo.workspace.style.LogoStyle.LOGO_LEFT;
import static gologolo.GoLogoLoPropertyType.ORDER_COLUMN;
import static gologolo.GoLogoLoPropertyType.SIZE_OPTIONS;
import gologolo.data.GoLogoLoData;
import gologolo.transactions.SortItems_Transaction;
import gologolo.workspace.controller.ItemsController;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_TOOLTIP;
import static gologolo.workspace.style.LogoStyle.LABEL_STYLE;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEMS_TABLE;
import static gologolo.GoLogoLoPropertyType.LOGO_MIDDLE_PANE;
import static gologolo.GoLogoLoPropertyType.RADIUS;
import static gologolo.GoLogoLoPropertyType.SIZE_DIALOG;
import static gologolo.GoLogoLoPropertyType.STOP_0;
import static gologolo.GoLogoLoPropertyType.STOP_1;
import gologolo.data.GoLogoLoCircle;
import gologolo.data.GoLogoLoRectangle;
import gologolo.workspace.foolproof.LogoSelectionFoolProofDesign;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_TEXT_FIELD;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_TABLE_X;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.scene.Node;


import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.shape.StrokeLineCap;
import static javafx.scene.shape.StrokeLineJoin.ROUND;
/**
 *
 * @author Qihong
 */
public class GoLogoLoWorkspace extends AppWorkspaceComponent{
    double orgSceneX, orgSceneY, orgTranslateX, orgTranslateY;
    GoLogoLoRectangle rec;
    GoLogoLoCircle cir;
    public GoLogoLoWorkspace(GoLogoLoApp app) {
        super(app);

        // LAYOUT THE APP
        initLayout();
        
        // 
        initFoolproofDesign();
    }
    private void initLayout() {
        // FIRST LOAD THE FONT FAMILIES FOR THE COMBO BOX
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // THIS WILL BUILD ALL OF OUR JavaFX COMPONENTS FOR US
        AppNodesBuilder logoBuilder = app.getGUIModule().getNodesBuilder();
        
        BorderPane logoPane = logoBuilder.buildBoderPane(CLASS_LOGO_PANE, null, null, CLASS_LOGO_BOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label logoLabel = logoBuilder.buildLabel(LOGO_LABEL,   logoPane,   null,   CLASS_LOGO_BIG_HEADER, HAS_KEY_HANDLER,       FOCUS_TRAVERSABLE,      ENABLED);
        
        BorderPane parent = logoBuilder.buildBoderPane(this, null, null, CLASS_LOGO_BOX_Y, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                
        VBox left = logoBuilder.buildVBox(this,null, null, CLASS_LOGO_BOX_X, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Pane middle = logoBuilder.buildPane(LOGO_MIDDLE_PANE,null, null, CLASS_LOGO_BOX_Y, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        VBox right = logoBuilder.buildVBox(this,null, null, CLASS_LOGO_BOX_Z, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        parent.setCenter(middle);
        
        parent.layoutBoundsProperty().addListener((ObservableValue<? extends Bounds> observable, Bounds oldBounds, Bounds bounds) -> {
            parent.setClip(new Rectangle(bounds.getMinX(), bounds.getMinY(), bounds.getWidth(), bounds.getHeight()));
        });
        
        middle.layoutBoundsProperty().addListener((ObservableValue<? extends Bounds> observable, Bounds oldBounds, Bounds bounds) -> {
            middle.setClip(new Rectangle(bounds.getMinX(), bounds.getMinY(), bounds.getWidth(), bounds.getHeight()));
        });
        
        right.setSpacing(10);
        HBox firstToolbar = logoBuilder.buildHBox(this, right, null, CLASS_LOGO_TOOL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button addTextButton = logoBuilder.buildIconButton(LOGO_ADD_TEXT_BUTTON, firstToolbar,  null,   CLASS_DJF_ICON_BUTTON,  HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
        Button addImageButton = logoBuilder.buildIconButton(ADD_IMAGE_BUTTON, firstToolbar,  null,   CLASS_DJF_ICON_BUTTON,  HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
        Button addTriangleButton = logoBuilder.buildIconButton(ADD_TRIANGLE_BUTTON, firstToolbar,  null,   CLASS_DJF_ICON_BUTTON,  HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
        Button addRectangleButton = logoBuilder.buildIconButton(ADD_RECTANGLE_BUTTON, firstToolbar,  null,   CLASS_DJF_ICON_BUTTON,  HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
        Button addCircleButton = logoBuilder.buildIconButton(ADD_CIRCLE_BUTTON, firstToolbar, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        
        FlowPane secondToolbar = logoBuilder.buildFlowPane(this, right, null, CLASS_LOGO_TOOL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        secondToolbar.setPrefWrapLength(250);
        secondToolbar.setHgap(5);
        ComboBox fontDialog = logoBuilder.buildComboBox(FONT_DIALOG, FONT_OPTIONS, DEFAULT_FONT, secondToolbar, null, LOGO_LEFT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        ComboBox fontSizeDialog = logoBuilder.buildComboBox(SIZE_DIALOG, SIZE_OPTIONS, DEFAULT_SIZES, secondToolbar, null, LOGO_LEFT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button boldButton = logoBuilder.buildIconButton(BOLD_BUTTON, secondToolbar, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button italicButton = logoBuilder.buildIconButton(ITALICIZE_BUTTON, secondToolbar, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button increaseFontButton = logoBuilder.buildIconButton(INCREASE_TEXT_BUTTON, secondToolbar, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button decreaseFontButton = logoBuilder.buildIconButton(DECREASE_TEXT_BUTTON, secondToolbar, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        ColorPicker underLineButton = logoBuilder.buildColorPicker(UNDERLINE_BUTTON, secondToolbar, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        TableView<GoLogoLoItemPrototype> itemsTable  = logoBuilder.buildTableView(LOGO_ITEMS_TABLE,    left,    null,   CLASS_LOGO_TABLE_X, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE,  true);
        TableColumn orderColumn      = logoBuilder.buildTableColumn(LOGO_ORDER_COLUMN,       itemsTable,        CLASS_LOGO_COLUMN);
        TableColumn nameColumn      = logoBuilder.buildTableColumn(LOGO_NAME_COLUMN,       itemsTable,         CLASS_LOGO_COLUMN);
        TableColumn typeColumn      = logoBuilder.buildTableColumn(LOGO_TYPE_COLUMN,       itemsTable,         CLASS_LOGO_COLUMN);
        
        orderColumn.setCellValueFactory(     new PropertyValueFactory<String,    String>("order"));
        nameColumn.setCellValueFactory(  new PropertyValueFactory<String,    String>("name"));
        typeColumn.setCellValueFactory(  new PropertyValueFactory<String,    String>("type"));
        
        HBox leftPane = logoBuilder.buildHBox(this, left, null, CLASS_LOGO_TOOL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button moveUpButton = logoBuilder.buildIconButton(MOVE_UP_BUTTON, leftPane,  null,   CLASS_DJF_ICON_BUTTON,  HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
        Button moveDownButton = logoBuilder.buildIconButton(MOVE_DOWN_BUTTON, leftPane,  null,   CLASS_DJF_ICON_BUTTON,  HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
        Button editButton = logoBuilder.buildIconButton(EDIT_ITEM_BUTTON, leftPane,  null,   CLASS_DJF_ICON_BUTTON,  HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
        Button deleteColumnButton = logoBuilder.buildIconButton(DELETE_COLUMN_BUTTON, leftPane,  null,   CLASS_DJF_ICON_BUTTON,  HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
        
        VBox thirdToolbar = logoBuilder.buildVBox(this, right, null, CLASS_LOGO_TOOL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        thirdToolbar.getChildren().add(new Label("Border Thickness:"));
        Slider BorderThickness = logoBuilder.buildSlider(BORDER_THICKNESS, thirdToolbar, null, null, 0, 20, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        thirdToolbar.getChildren().add(new Label("Border Color:"));
        ColorPicker colorDialog = logoBuilder.buildColorPicker(BORDER_COLOR, thirdToolbar, null, LOGO_LEFT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        thirdToolbar.getChildren().add(new Label("Border Radius:"));
        Slider BorderRadius = logoBuilder.buildSlider(BORDER_RADIUS, thirdToolbar, null, null, 0, 100, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        VBox fourToolbar = logoBuilder.buildVBox(this, right, null, CLASS_LOGO_TOOL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        Label label = new Label("Color Gradient");
        label.setStyle("-fx-font: 24 arial;");
        fourToolbar.getChildren().add(label);
        
        fourToolbar.getChildren().add(new Label("Focus Angle:"));
        Slider FocusAngle = logoBuilder.buildSlider(FOCUS_ANGLE, fourToolbar, null, null, 0, 10, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        fourToolbar.getChildren().add(new Label("Focus Distance:"));
        Slider FocusDistance = logoBuilder.buildSlider(FOCUS_DISTANCE, fourToolbar, null, null, 0, 10, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        fourToolbar.getChildren().add(new Label("Center X:"));
        Slider xCenter = logoBuilder.buildSlider(CENTER_X, fourToolbar, null, null, 0, 10, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        fourToolbar.getChildren().add(new Label("Center Y:"));
        Slider yCenter = logoBuilder.buildSlider(CENTER_Y, fourToolbar, null, null, 0, 10, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        fourToolbar.getChildren().add(new Label("Radius:"));
        Slider radius = logoBuilder.buildSlider(RADIUS, fourToolbar, null, null, 0, 10, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        fourToolbar.getChildren().add(new Label("Cycle Method:"));
        ComboBox cycleDialog = logoBuilder.buildComboBox(CYCLE_METHOD, "CYCLE_OPTIONS", "DEFAULT_CYCLE", fourToolbar, null, LOGO_LEFT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        fourToolbar.getChildren().add(new Label("Stop 0 Color:"));
        ColorPicker stop0Dialog = logoBuilder.buildColorPicker(STOP_0, fourToolbar, null, LOGO_LEFT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        fourToolbar.getChildren().add(new Label("Stop 1 Color:"));
        ColorPicker stop1Dialog = logoBuilder.buildColorPicker(STOP_1, fourToolbar, null, LOGO_LEFT, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        Button resize = (Button) app.getGUIModule().getGUINode(EXPAND_BUTTON);
        Button zoomIn = (Button) app.getGUIModule().getGUINode(ZOOM_IN_BUTTON);
        Button zoomOut = (Button) app.getGUIModule().getGUINode(ZOOM_OUT_BUTTON);
        Button reset = (Button) app.getGUIModule().getGUINode(HOME_BUTTON);
        logoPane.setLeft(left);
        logoPane.setCenter(parent);
        logoPane.setRight(right);
        workspace = new BorderPane();
	((BorderPane)workspace).setCenter(logoPane);
        
        ItemsController itemsController = new ItemsController((GoLogoLoApp)app);
        
        addTextButton.setOnAction(e->{
            itemsController.processAddText();
        });
        addCircleButton.setOnAction(e->{
            itemsController.processAddCircle();
        });
        addImageButton.setOnAction(e->{
            try {
                itemsController.processAddImage();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GoLogoLoWorkspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        cycleDialog.setOnAction(e->{
            itemsController.processCycleMethod((String)cycleDialog.getSelectionModel().getSelectedItem());
        });
        fontDialog.setOnAction(e->{
            itemsController.processChangeFont((String)fontDialog.getSelectionModel().getSelectedItem());
        });
        fontSizeDialog.setOnAction(e->{
            itemsController.processChangeSize((String)fontSizeDialog.getSelectionModel().getSelectedItem());
        });
        colorDialog.setOnAction(ee->{
            itemsController.processBorderColor(colorDialog.getValue());
        });
        stop0Dialog.setOnAction(e->{
            itemsController.processStop0(stop0Dialog.getValue());
        });
        stop1Dialog.setOnAction(e->{
            itemsController.processStop1(stop1Dialog.getValue());
        });
        underLineButton.setOnAction(e->{
            itemsController.processUnderLine(underLineButton.getValue());
        });
        addRectangleButton.setOnAction(e->{
            itemsController.processAddRectangle();
        });
        moveUpButton.setOnAction(e->{
            itemsController.processMoveUp();
        });
        moveDownButton.setOnAction(e->{
            itemsController.processMoveDown();
        });
        editButton.setOnAction(e->{
            itemsController.processEditItems();
        });
        deleteColumnButton.setOnAction(e->{
            itemsController.processRemoveItems();
        });
        increaseFontButton.setOnAction(e->{
            itemsController.processIncreaseFont();
        });
        decreaseFontButton.setOnAction(e->{
            itemsController.processDecreaseFont();
        });
        boldButton.setOnAction(e->{
            itemsController.processBold();
        });
        italicButton.setOnAction(e->{
            itemsController.processItalic();
        });
        zoomIn.setOnAction(e->{
            double scaleX = middle.getScaleX()*1.1;
            double scaleY = middle.getScaleY()*1.1;
            middle.setScaleX(scaleX);
            middle.setScaleY(scaleY);
        });
        zoomOut.setOnAction(e->{
            double scaleX = middle.getScaleX()*0.9;
            double scaleY = middle.getScaleY()*0.9;
            middle.setScaleX(scaleX);
            middle.setScaleY(scaleY);
        });
        reset.setOnAction(e->{
            middle.setScaleX(1.0);
            middle.setScaleY(1.0);
        });
        BorderThickness.valueProperty().addListener(e->{
            Shape shape;
            GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
            int index = data.getItems().indexOf(data.getSelectedItem());
            shape = (Shape) data.getNode(index);
            BorderThickness.setOnMouseDragged(z->{
                shape.setStrokeWidth(BorderThickness.valueProperty().doubleValue());
            });
            BorderThickness.setOnMouseReleased(x->{
                itemsController.processBorderThickness(BorderThickness.valueProperty().doubleValue());
            });
        });
        BorderRadius.valueProperty().addListener(e->{
            GoLogoLoRectangle rec;
            GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
            int index = data.getItems().indexOf(data.getSelectedItem());
            rec = (GoLogoLoRectangle) data.getNode(index);
            BorderRadius.setOnMouseDragged(z->{
                rec.setArcHeight(BorderRadius.valueProperty().doubleValue());
                rec.setArcWidth(BorderRadius.valueProperty().doubleValue());
            });
            BorderRadius.setOnMouseReleased(x->{
                itemsController.processBorderRadius(BorderRadius.valueProperty().doubleValue());
            });
        });
        FocusAngle.valueProperty().addListener(e->{
            FocusAngle.setOnMouseReleased(x->{
                itemsController.processFocusAngle(FocusAngle.valueProperty().doubleValue());
            });
            GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
            FocusAngle.setOnMouseDragged(z->{
                if(data.getSelectedItem().isIsCircle()){
                    cir = (GoLogoLoCircle) data.getNode(data.getItemIndex(data.getSelectedItem()));
                    RadialGradient gradient = new RadialGradient(FocusAngle.valueProperty().doubleValue(), cir.getFocusDistance(), 
                                          cir.getCenterX(), cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), cir.getStop1());
                    cir.setFill(gradient);
                }
                else{
                RadialGradient gradient = new RadialGradient(FocusAngle.valueProperty().doubleValue(), rec.getFocusDistance(), 
                                          rec.getCenterX(), rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), rec.getStop1());
                rec.setFill(gradient);
                }
            });
        });
        FocusDistance.valueProperty().addListener(e->{
            FocusDistance.setOnMouseReleased(x->{
                itemsController.processFocusDistance(FocusDistance.valueProperty().doubleValue());
            });
            GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
            FocusDistance.setOnMouseDragged(z->{
                if(data.getSelectedItem().isIsCircle()){
                    cir = (GoLogoLoCircle) data.getNode(data.getItemIndex(data.getSelectedItem()));
                    RadialGradient gradient = new RadialGradient(cir.getFocusAngle(), FocusDistance.valueProperty().doubleValue(), 
                                          cir.getCenterX(), cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), cir.getStop1());
                    cir.setFill(gradient);
                }
                else{
                RadialGradient gradient = new RadialGradient(rec.getFocusAngle(), FocusDistance.valueProperty().doubleValue(), 
                                          rec.getCenterX(), rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), rec.getStop1());
                rec.setFill(gradient);
                }
            });
        });
        xCenter.valueProperty().addListener(e->{
            xCenter.setOnMouseReleased(x->{
                itemsController.processX(xCenter.valueProperty().doubleValue());
            });
            GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
            
            xCenter.setOnMouseDragged(z->{
                if(data.getSelectedItem().isIsCircle()){
                    cir = (GoLogoLoCircle) data.getNode(data.getItemIndex(data.getSelectedItem()));
                    RadialGradient gradient = new RadialGradient(cir.getFocusAngle(), cir.getFocusDistance(), 
                                          xCenter.valueProperty().doubleValue(), cir.getRadialY(),
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), cir.getStop1());
                    cir.setFill(gradient);
                }
                else{
                RadialGradient gradient = new RadialGradient(rec.getFocusAngle(), rec.getFocusDistance(), 
                                          xCenter.valueProperty().doubleValue(), rec.getCenterY(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), rec.getStop1());
                rec.setFill(gradient);
                 }
            });
        });
        yCenter.valueProperty().addListener(e->{
            yCenter.setOnMouseReleased(x->{
                itemsController.processY(yCenter.valueProperty().doubleValue());
            });
            GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
            
            yCenter.setOnMouseDragged(z->{
                if(data.getSelectedItem().isIsCircle()){
                    cir = (GoLogoLoCircle) data.getNode(data.getItemIndex(data.getSelectedItem()));
                    RadialGradient gradient = new RadialGradient(cir.getFocusAngle(), cir.getFocusDistance(), 
                                          cir.getRadialX(), yCenter.valueProperty().doubleValue(),
                                            cir.getRadialRadius(), cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), cir.getStop1());
                    cir.setFill(gradient);
                }
                else{
                    rec = (GoLogoLoRectangle) data.getNode(data.getItemIndex(data.getSelectedItem()));
                    RadialGradient gradient = new RadialGradient(rec.getFocusAngle(), rec.getFocusDistance(), 
                                          rec.getCenterX(), yCenter.valueProperty().doubleValue(),
                                            rec.getRadius(), rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), rec.getStop1());
                    rec.setFill(gradient);
                }
            });
        });
        radius.valueProperty().addListener(e->{
            radius.setOnMouseReleased(x->{
                itemsController.processRadius(radius.valueProperty().doubleValue());
            });
            GoLogoLoData data = (GoLogoLoData) app.getDataComponent();
            radius.setOnMouseDragged(z->{
                if(data.getSelectedItem().isIsCircle()){
                    cir = (GoLogoLoCircle) data.getNode(data.getItemIndex(data.getSelectedItem()));
                    RadialGradient gradient = new RadialGradient(cir.getFocusAngle(), cir.getFocusDistance(), 
                                          cir.getRadialX(), cir.getRadialY(),
                                            radius.valueProperty().doubleValue(), cir.isProportional(), cir.getCycleMethod(), cir.getStop0(), cir.getStop1());
                    cir.setFill(gradient);
                }
                else{
                RadialGradient gradient = new RadialGradient(rec.getFocusAngle(), rec.getFocusDistance(), 
                                          rec.getCenterX(), rec.getCenterY(),
                                            radius.valueProperty().doubleValue(), rec.isProportional(), rec.getCycleMethod(), rec.getStop0(), rec.getStop1());
                rec.setFill(gradient);
                }
            });
        });
        resize.setOnAction(e->{
            itemsController.processResize();
        });
        itemsTable.setOnMouseClicked(e -> {
            if(e.getClickCount()==2){
                itemsController.processEditText();
            }
             app.getFoolproofModule().updateAll();

        });
        middle.setOnMouseEntered(e-> {
            //addMouseScrolling(middle);
            
            middle.setOnMouseClicked(x-> {
                
            });
            middle.setOnMouseDragged(z->{
                
            });
        });
        ItemsTableController iTC = new ItemsTableController(app);
        itemsTable.setOnSort(new EventHandler<SortEvent<TableView<GoLogoLoItemPrototype>>>(){
            @Override
            public void handle(SortEvent<TableView<GoLogoLoItemPrototype>> event) {
                GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
                ArrayList<GoLogoLoItemPrototype> oldListOrder = data.getCurrentItemsOrder();
                TableView view = event.getSource();
                ObservableList sortOrder = view.getSortOrder();
                if ((sortOrder != null) && (sortOrder.size() == 1)) {
                    TableColumn sortColumn = event.getSource().getSortOrder().get(0);
                    String columnText = sortColumn.getText();
                    SortType sortType = sortColumn.getSortType();
                    System.out.println("Sort by " + columnText);
                    event.consume();
                    SortItems_Transaction transaction = new SortItems_Transaction(data, oldListOrder, columnText, sortType);
                    app.processTransaction(transaction);
                    app.getFoolproofModule().updateAll();
                }
            }            
        });
    }

    public void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
         foolproofSettings.registerModeSettings(LOGO_FOOLPROOF_SETTINGS, 
                new LogoSelectionFoolProofDesign((GoLogoLoApp)app));

    }
    
    public void addMouseScrolling(Node node) {
        node.setOnScroll((ScrollEvent event) -> {
            // Adjust the zoom factor as per your requirement
            double zoomFactor = 1.05;
            double deltaY = event.getDeltaY();
            if (deltaY < 0){
                zoomFactor = 2.0 - zoomFactor;
            }
            node.setScaleX(node.getScaleX() * zoomFactor);
            node.setScaleY(node.getScaleY() * zoomFactor);
        });
    }

    @Override
    public void processWorkspaceKeyEvent(KeyEvent ke) {
        
    }

    @Override
    public void showNewDialog() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
