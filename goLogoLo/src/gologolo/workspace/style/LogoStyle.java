/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.workspace.style;

/**
 *
 * @author Qihong
 */
public class LogoStyle {
    public static final String EMPTY_TEXT = "";
    public static final int BUTTON_TAG_WIDTH = 75;

    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS M3Workspace'S COMPONENTS TO A STYLE SHEET THAT IT USES
    // NOTE THAT FOUR CLASS STYLES ALREADY EXIST:
    // top_toolbar, toolbar, toolbar_text_button, toolbar_icon_button
    
    public static final String CLASS_LOGO_PANE          = "logo_pane";
    public static final String CLASS_LOGO_BOX           = "logo_box";            
    public static final String CLASS_LOGO_BIG_HEADER    = "logo_big_header";
    public static final String CLASS_LOGO_SMALL_HEADER  = "logo_small_header";
    public static final String CLASS_LOGO_PROMPT        = "logo_prompt";
    public static final String CLASS_LOGO_TEXT_FIELD    = "logo_text_field";
    public static final String CLASS_LOGO_BUTTON        = "logo_button";
    public static final String CLASS_LOGO_TABLE         = "logo_table";
    public static final String CLASS_LOGO_COLUMN        = "logo_column";
    // STYLE CLASSES FOR THE ADD/EDIT ITEM DIALOG
    public static final String CLASS_LOGO_DIALOG_GRID           = "logo_dialog_grid";
    public static final String CLASS_LOGO_DIALOG_HEADER         = "logo_dialog_header";
    public static final String CLASS_LOGO_DIALOG_PROMPT         = "logo_dialog_prompt";
    public static final String CLASS_LOGO_DIALOG_TEXT_FIELD     = "logo_dialog_text_field";
    public static final String CLASS_LOGO_DIALOG_DATE_PICKER    = "logo_dialog_date_picker";
    public static final String CLASS_LOGO_DIALOG_CHECK_BOX      = "logo_dialog_check_box";
    public static final String CLASS_LOGO_DIALOG_BUTTON         = "logo_dialog_button";
    public static final String CLASS_LOGO_DIALOG_PANE           = "logo_dialog_pane";
    public static final String LOGO_LEFT = "logo_left";
    public static final String LABEL_STYLE = "label_style";
    public static final String CLASS_LOGO_BOX_X = "logo_box_x";
    public static final String CLASS_LOGO_TABLE_X = "logo_table_x";
    public static final String CLASS_LOGO_BOX_Y = "logo_box_y";
    public static final String CLASS_LOGO_BOX_Z = "logo_box_z";
    public static final String CLASS_LOGO_TOOL = "logo_toolbar";
    public static final String CLASS_LOGO_TOOLTIP = "tooltip";
    public static final String CLASS_LOGO_RECTANGLE = "rectanle";
    public static final String CLASS_LOGO_RECTANGLES = "rectanles";
    public static final String CLASS_LOGO_DIALOG_TEXT = "texts";
    public static final String CLASS_LOGO_TEXT_ITALIC = "text_italic";
    public static final String CLASS_LOGO_TEXT_BOLD = "text_bold";
}
