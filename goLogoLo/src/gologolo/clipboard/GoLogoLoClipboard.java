/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.clipboard;

import djf.components.AppClipboardComponent;
import java.util.ArrayList;
import gologolo.GoLogoLoApp;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoItemPrototype;
import gologolo.transactions.CutItems_Transaction;
import gologolo.transactions.PasteItems_Transaction;
import javafx.scene.Node;
/**
 *
 * @author Qihong
 */
public class GoLogoLoClipboard implements AppClipboardComponent{
    GoLogoLoApp app;
    GoLogoLoData data;
    ArrayList<GoLogoLoItemPrototype> clipboardCutItems;
    ArrayList<GoLogoLoItemPrototype> clipboardCopiedItems;
    ArrayList<Node> clipboardCutNodes;
    ArrayList<Node> clipboardCopiedNodes;
    public GoLogoLoClipboard(GoLogoLoApp initApp) {
        app = initApp;
        clipboardCutItems = null;
        clipboardCopiedItems = null;
        clipboardCutNodes = null;
        clipboardCopiedNodes = null;
    }
    
    @Override
    public void cut(){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if (data.isItemSelected() || data.areItemsSelected()) {
            clipboardCutItems = new ArrayList(data.getSelectedItems());
            clipboardCopiedItems = null;
            clipboardCutNodes = new ArrayList(data.getSelectedComponents());
            clipboardCopiedNodes = null;
            CutItems_Transaction transaction = new CutItems_Transaction((GoLogoLoApp)app, clipboardCutItems,clipboardCutNodes);
            app.processTransaction(transaction);
        }
    }
    
    @Override
    public void copy(){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if (data.isItemSelected() || data.areItemsSelected()) {
            clipboardCopiedItems = new ArrayList(data.getSelectedItems());
            clipboardCutItems = null;
            clipboardCopiedNodes = new ArrayList(data.getSelectedComponents());
            clipboardCutNodes = null;
        }
    }
    @Override
    public void paste(){
        GoLogoLoData data = (GoLogoLoData)app.getDataComponent();
        if (data.isItemSelected()) {
            int selectedIndex = data.getItemIndex(data.getSelectedItem());  
            ArrayList<GoLogoLoItemPrototype> pasteItems = clipboardCutItems;
            if ((clipboardCutItems != null)
                    && (!clipboardCutItems.isEmpty())) {
                PasteItems_Transaction transaction = new PasteItems_Transaction((GoLogoLoApp)app, clipboardCutItems, clipboardCutNodes, selectedIndex, false);
                app.processTransaction(transaction);
                
                // NOW WE HAVE TO RE-COPY THE CUT ITEMS TO MAKE
                // SURE IF WE PASTE THEM AGAIN THEY ARE BRAND NEW OBJECTS
                copyToCutClipboard(clipboardCutItems);
                copyToCutNodeClipboard(clipboardCutNodes);
            }
            else if ((clipboardCopiedItems != null)
                    && (!clipboardCopiedItems.isEmpty())) {
                PasteItems_Transaction transaction = new PasteItems_Transaction((GoLogoLoApp)app, clipboardCopiedItems, clipboardCopiedNodes, selectedIndex, true);
                app.processTransaction(transaction);
            
                // NOW WE HAVE TO RE-COPY THE COPIED ITEMS TO MAKE
                // SURE IF WE PASTE THEM AGAIN THEY ARE BRAND NEW OBJECTS
                copyToCopiedClipboard(clipboardCopiedItems);
                copyToCopiedNodeClipboard(clipboardCopiedNodes);
            }
        }
    }
    
    private void copyToCutClipboard(ArrayList<GoLogoLoItemPrototype> itemsToCopy) {
        clipboardCutItems = copyItems(itemsToCopy);
        clipboardCopiedItems = null;        
    }
    
    private void copyToCutNodeClipboard(ArrayList<Node> itemsToCopy) {
        clipboardCutNodes = copyNodes(itemsToCopy);
        clipboardCopiedNodes = null;        
    }
    
    private void copyToCopiedNodeClipboard(ArrayList<Node> itemsToCopy) {
        clipboardCutNodes = null;
        clipboardCopiedNodes = copyNodes(itemsToCopy);        
    }
    
    private void copyToCopiedClipboard(ArrayList<GoLogoLoItemPrototype> itemsToCopy) {
        clipboardCutItems = null;
        clipboardCopiedItems = copyItems(itemsToCopy);        
    }
    
    private void copyNodesToCopiedClipboard(ArrayList<Node> itemsToCopy) {
        clipboardCutNodes = null;
        clipboardCopiedNodes = copyNodes(itemsToCopy);      
    }
    
    private ArrayList<GoLogoLoItemPrototype> copyItems(ArrayList<GoLogoLoItemPrototype> itemsToCopy) {
        ArrayList<GoLogoLoItemPrototype> tempCopy = new ArrayList();         
        for (GoLogoLoItemPrototype itemToCopy : itemsToCopy) {
            GoLogoLoItemPrototype copiedItem = (GoLogoLoItemPrototype)itemToCopy.clone();
            tempCopy.add(copiedItem);
        }        
        return tempCopy;
    }
    
    private ArrayList<Node> copyNodes(ArrayList<Node> itemsToCopy) {
        ArrayList<Node> tempCopy = new ArrayList();         
        for (Node itemToCopy : itemsToCopy) {
            Node copiedNode = (Node)itemToCopy;
            tempCopy.add(copiedNode);
        }        
        return tempCopy;
    }
    
    @Override
    public boolean hasSomethingToCut() {
        return true;
    }

    @Override
    public boolean hasSomethingToCopy() {
       return true;
    }

    @Override
    public boolean hasSomethingToPaste() {
        return true;
    }
}
