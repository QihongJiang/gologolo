/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data;
import javafx.scene.shape.Polygon;
/**
 *
 * @author Qihong
 */
public class GoLogoLoTriangle extends Polygon{
    double x;
    double y;
    double side;

    GoLogoLoTriangle(double x, double y, double side){
        this.x = x;
        this.y = y;
        this.side = side;

        setLayoutX(x);
        setLayoutY(y);
        getPoints().addAll(
                x, (-(Math.sqrt((side*side)-((side/2)*(side/2)))/2)),
                (side/2), Math.sqrt((side*side)-((side/2)*(side/2)))/2,
                (-(side/2)), Math.sqrt((side*side)-((side/2)*(side/2)))/2);
    }
}
