/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data;

import gologolo.transactions.DragAndMove_Transaction;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_RECTANGLE;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_RECTANGLES;
import java.util.List;
import javafx.scene.paint.Color;
import static javafx.scene.paint.Color.BLACK;
import static javafx.scene.paint.Color.WHITE;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author qihong
 */
public class GoLogoLoRectangle extends Rectangle{
    Rectangle rec;
    double borderThickness;
    double borderRadius;
    double width;
    double height;
    double xPos;
    double yPos;
    double focusAngle;
    double focusDistance;
    double centerX;
    double centerY;
    double radius;
    Color borderColor;boolean proportional;
    CycleMethod cycleMethod;
    List<Stop>stops;
    Stop stop0;
    Stop stop1;
    RadialGradient gradient;
    
    public GoLogoLoRectangle(double width, double height, double xPos, double yPos){
        rec = new Rectangle();
        rec.setStroke(BLACK);
        
        rec.setFill(WHITE);
        this.setWidth(width);
        this.setHeight(height);
        this.setXPos(xPos);
        this.setYPos(yPos);
        this.setBorderColor(BLACK);
        this.setStop0(new Stop(0,WHITE));
        this.setStop1(new Stop(1,WHITE));
        this.width = width;
        this.height = height;
        borderThickness = 1;
        borderRadius = 0;
        focusAngle = 0;
        focusDistance = 0;
        centerX = 0;
        centerY = 0;
        radius = 0;
        borderColor = BLACK;
        proportional = true;
        stop0 = getStop0();
        stop1 = getStop1();
        cycleMethod = CycleMethod.NO_CYCLE;
    }
    
    public GoLogoLoRectangle(double width, double height, double xPos, double yPos,
                                double borderT, double borderR, Color borderC,
                                double focusA, double focusD, double cx, double cy, double r,
                                Stop stop_0, Stop stop_1, CycleMethod c
                                ){
        rec = new Rectangle();
        rec.setStroke(BLACK);
        
        this.setWidth(width);
        this.setHeight(height);
        this.setXPos(xPos);
        this.setYPos(yPos);
        this.width = width;
        this.height = height;
        borderThickness = borderT;
        borderRadius = borderR;
        borderColor = borderC;
        focusAngle = focusA;
        focusDistance = focusD;
        centerX = cx;
        centerY = cy;
        radius = r;
        proportional = true;
        stop0 = stop_0;
        stop1 = stop_1;
        cycleMethod = c;
        gradient = new RadialGradient(focusA, focusD, 
                                          cx, cy,
                                            r, true, c, stop_0, stop_1);
    }
    
    public double getBorderThickness() {
        return borderThickness;
    }

    public void setBorderThickness(double borderThickness) {
        this.borderThickness = borderThickness;
    }

    public double getBorderRadius() {
        return borderRadius;
    }

    public RadialGradient getGradient() {
        return gradient;
    }

    public void setGradient(RadialGradient gradient) {
        this.gradient = gradient;
    }
    
    
    
    public Color getBorderColor() {
        return borderColor;
    }
    
    public Color getStop0Color(){
        return stop0.getColor();
    }
    
    public Color getStop1Color(){
        return stop1.getColor();
    }

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }

    public void setBorderRadius(double borderRadius) {
        this.borderRadius = borderRadius;
    }
    
    public Stop getStop0() {
        return stop0;
    }

    public void setStop0(Stop stop0) {
        this.stop0 = stop0;
    }

    public Stop getStop1() {
        return stop1;
    }

    public void setStop1(Stop stop1) {
        this.stop1 = stop1;
    }
    
    public List<Stop> getStops(){
        return stops;
    }
    
    public boolean isProportional() {
        return proportional;
    }

    public void setProportional(boolean proportional) {
        this.proportional = proportional;
    }
    
    public double getFocusAngle(){
        return focusAngle;
    }
    
    public void setFocusAngle(double value){
        focusAngle = value;
    }
    
    public double getFocusDistance() {
        return focusDistance;
    }

    public void setFocusDistance(double focusDistance) {
        this.focusDistance = focusDistance;
    }

    public double getCenterX() {
        return centerX;
    }

    public void setCenterX(double centerX) {
        this.centerX = centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    public void setCenterY(double centerY) {
        this.centerY = centerY;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public CycleMethod getCycleMethod() {
        return cycleMethod;
    }

    public void setCycleMethod(CycleMethod cycleMethod) {
        this.cycleMethod = cycleMethod;
    }
    
    public double getXPos(){
        return xPos;
    }
    
    public double getYPos(){
        return yPos;
    }
    
    public void setXPos(double value){
        this.xPos = value;
        rec.setX(xPos);
    }
    
    public void setYPos(double value){
        this.yPos = value;
        rec.setY(yPos);
    }
    
    public Object clone() {
        return rec = new GoLogoLoRectangle(width, height, xPos, yPos, 
                borderThickness,borderRadius, borderColor, focusAngle, focusDistance, centerX, centerY, radius,
                stop0, stop1, cycleMethod);
    }
    
    public boolean equals(Object obj) {
        return this == obj;
    }
}
