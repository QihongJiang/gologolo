/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data;

import javafx.scene.image.ImageView;

/**
 *
 * @author qihong
 */
public class GoLogoLoImage extends ImageView{
    ImageView imageView;
    double xPos;
    double yPos;
    public GoLogoLoImage(ImageView initImage, double xPos, double yPos){
        imageView = initImage;
        imageView.setX(xPos);
        imageView.setY(yPos);
        this.xPos = xPos;
        this.yPos = yPos;
    }
    
    public double getXPos(){
        return xPos;
    }
    
    public double getYPos(){
        return yPos;
    }
    
    public ImageView getImageV(){
        return imageView;
    }
    
    public Object clone(){
        return new GoLogoLoImage(getImageV(), getXPos(), getYPos());
    }
        
}
