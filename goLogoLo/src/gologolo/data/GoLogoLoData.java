/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data;

import djf.components.AppDataComponent;
import djf.modules.AppGUIModule;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import gologolo.GoLogoLoApp;
import static gologolo.GoLogoLoPropertyType.BORDER_RADIUS;
import static gologolo.GoLogoLoPropertyType.BORDER_THICKNESS;
import static gologolo.GoLogoLoPropertyType.CENTER_X;
import static gologolo.GoLogoLoPropertyType.CENTER_Y;
import static gologolo.GoLogoLoPropertyType.CYCLE_METHOD;
import static gologolo.GoLogoLoPropertyType.FOCUS_ANGLE;
import static gologolo.GoLogoLoPropertyType.FOCUS_DISTANCE;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import static gologolo.GoLogoLoPropertyType.LOGO_ITEMS_TABLE;
import static gologolo.GoLogoLoPropertyType.LOGO_MIDDLE_PANE;
import static gologolo.GoLogoLoPropertyType.RADIUS;
import static gologolo.GoLogoLoPropertyType.STOP_0;
import static gologolo.GoLogoLoPropertyType.STOP_1;
import gologolo.transactions.DragAndMove_Transaction;
import gologolo.workspace.controller.ItemsController;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_RECTANGLE;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_RECTANGLES;
import javafx.collections.FXCollections;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.Slider;
import javafx.scene.effect.Glow;
import javafx.scene.image.ImageView;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.RadialGradient;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import properties_manager.PropertiesManager;
/**
 *
 * @author Qihong
 */
public class GoLogoLoData implements AppDataComponent{
    double orgSceneX, orgSceneY, orgTranslateX, orgTranslateY;
    ColorPicker colorPickerStop0, colorPickerStop1;
    ComboBox cycleMethod;
    Slider focusDistance, focusAngle, centerX, centerY, radius;
    Slider borderThickness, borderRadius;
    GoLogoLoApp app;
    ObservableList<GoLogoLoItemPrototype> items;
    ObservableList<Node>nodes;
    ArrayList<Node>Selectednodes;
    ArrayList<Node>tempNodes;
    TableView.TableViewSelectionModel itemsSelectionModel;
    SelectionModel componentSelectionModel;
    Node selectedNode;
    Boolean isNodeSelected;
    
    public GoLogoLoData(GoLogoLoApp initApp) {
        app = initApp;
        TableView tableView = (TableView) app.getGUIModule().getGUINode(LOGO_ITEMS_TABLE);
        Pane pane = (Pane) app.getGUIModule().getGUINode(LOGO_MIDDLE_PANE);
        items = tableView.getItems();
        nodes = pane.getChildren();
        tempNodes = new ArrayList<Node>();
        itemsSelectionModel = tableView.getSelectionModel();
        itemsSelectionModel.setSelectionMode(SelectionMode.MULTIPLE);
        
        borderThickness = (Slider) app.getGUIModule().getGUINode(BORDER_THICKNESS);
        borderRadius = (Slider) app.getGUIModule().getGUINode(BORDER_RADIUS);
        cycleMethod = (ComboBox) app.getGUIModule().getGUINode(CYCLE_METHOD);
        
        colorPickerStop0 = (ColorPicker) app.getGUIModule().getGUINode(STOP_0);
        colorPickerStop1 = (ColorPicker) app.getGUIModule().getGUINode(STOP_1);
        
        focusDistance = (Slider) app.getGUIModule().getGUINode(FOCUS_DISTANCE);
        focusAngle = (Slider) app.getGUIModule().getGUINode(FOCUS_ANGLE);
        centerX = (Slider) app.getGUIModule().getGUINode(CENTER_X);
        centerY = (Slider) app.getGUIModule().getGUINode(CENTER_Y);
        radius = (Slider) app.getGUIModule().getGUINode(RADIUS);
    }
    
    public Iterator<GoLogoLoItemPrototype> itemsIterator() {
        return this.items.iterator();
    }
    
    public Iterator<Node> nodesIterator() {
        return this.nodes.iterator();
    }
    
    public boolean isItemSelected() {
        ObservableList<GoLogoLoItemPrototype> selectedItems = this.getSelectedItems();
        return (selectedItems != null) && (selectedItems.size() == 1);
    }
    
    public boolean areItemsSelected() {
        ObservableList<GoLogoLoItemPrototype> selectedItems = this.getSelectedItems();
        return (selectedItems != null) && (selectedItems.size() > 1);        
    }

    public boolean isValidEditItem(GoLogoLoItemPrototype itemToEdit, int order, String name) {
        return isValidNewGoLogoLoItem(order, name);
    }

    public boolean isValidNewGoLogoLoItem(int order, String name) {
        if (name.trim().length() == 0)
            return false;
        return true;
    }
    
    public void addItem(GoLogoLoItemPrototype itemToAdd) {
        items.add(itemToAdd);
    }

    public void removeItem(GoLogoLoItemPrototype itemToAdd) {
        items.remove(itemToAdd);
    }
    
    public void addNode(Node node){
        nodes.add(node);
        int index = nodes.indexOf(node);
        Bounds bounds = node.getBoundsInParent();
        
        ItemsController itemsController = new ItemsController((GoLogoLoApp)app);
        if(items.get(index).getIsText()){
            
            GoLogoLoText text = (GoLogoLoText) node;
            
            text.setOnMouseClicked(x-> {
                this.clearSelected();
                this.selectItem(items.get(nodes.indexOf(node)));
                
                if(x.getClickCount()==2&&items.get(this.getNodeIndex(node)).isTextField){                
                    itemsController.processEditText();
                }
            });
        
        
            text.setOnMousePressed(e->{
                this.clearSelected();
                this.selectItem(items.get(nodes.indexOf(node)));
                this.isNodeSelected = true;
                orgSceneX = e.getSceneX();
                orgSceneY = e.getSceneY();
                orgTranslateX = text.getTranslateX();
                orgTranslateY = text.getTranslateY();
            });
        
            text.setOnMouseDragged(e->{
                double offsetX = e.getSceneX() - orgSceneX;
                double offsetY = e.getSceneY() - orgSceneY;
                double newTranslateX = orgTranslateX + offsetX;
                double newTranslateY = orgTranslateY + offsetY;

                text.setTranslateX(newTranslateX);
                text.setTranslateY(newTranslateY);
                text.setOnMouseReleased(x->{
                    DragAndMove_Transaction transaction = new DragAndMove_Transaction(text, orgTranslateX, orgTranslateY, newTranslateX, newTranslateY);
                    app.processTransaction(transaction);
                });
            });
        }
        else if(items.get(index).getIsShape()){
            GoLogoLoRectangle rect = (GoLogoLoRectangle) node;
            
            rect.setOnMouseClicked(x-> {
                this.clearSelected();
                this.selectItem(items.get(nodes.indexOf(node)));
                
                borderThickness.adjustValue(rect.getBorderThickness());
                borderRadius.adjustValue(rect.getBorderRadius());
                
                focusAngle.adjustValue(rect.getFocusAngle());
                focusDistance.adjustValue(rect.getFocusDistance());
                centerX.adjustValue(rect.getCenterX());
                centerY.adjustValue(rect.getCenterY());
                radius.adjustValue(rect.getRadius());
                cycleMethod.setValue(rect.getCycleMethod());
                colorPickerStop0.setValue(rect.getStop0Color());
                colorPickerStop1.setValue(rect.getStop1Color());
                
                if(x.getClickCount()==2&&items.get(this.getNodeIndex(node)).isTextField){                
                    itemsController.processEditText();
                }
            });
            
            rect.setOnMouseEntered(e->{
                if(isItemSelected()){
                    rect.setOnScroll((ScrollEvent w) ->{
                        double zoomFactor = 1.05;
                        double deltaY = w.getDeltaY();
                        if (deltaY < 0){
                            zoomFactor = 2.0 - zoomFactor;
                    }
                        rect.setScaleX(rect.getScaleX() * zoomFactor);
                        rect.setScaleY(rect.getScaleY() * zoomFactor);
                    });
                }
            });
        
            rect.setOnMousePressed(e->{
                this.clearSelected();
                this.selectedNode = rect;
                Glow glow = new Glow();
                rect.setEffect(glow);
                this.selectItem(items.get(nodes.indexOf(node)));
                orgSceneX = e.getSceneX();
                orgSceneY = e.getSceneY();
                orgTranslateX = rect.getTranslateX();
                orgTranslateY = rect.getTranslateY();
            });
        
            rect.setOnMouseDragged(e->{
                double offsetX = e.getSceneX() - orgSceneX;
                double offsetY = e.getSceneY() - orgSceneY;
                double newTranslateX = orgTranslateX + offsetX;
                double newTranslateY = orgTranslateY + offsetY;

                rect.setTranslateX(newTranslateX);
                rect.setTranslateY(newTranslateY);
                rect.setOnMouseReleased(x->{
                    rect.setEffect(null);
                    DragAndMove_Transaction transaction = new DragAndMove_Transaction(rect, orgTranslateX, orgTranslateY, newTranslateX, newTranslateY);
                    app.processTransaction(transaction);
                });
            });
        }
        else if(items.get(index).isIsCircle()){
            GoLogoLoCircle rect = (GoLogoLoCircle) node;
            
            rect.setOnMouseClicked(x-> {
                this.clearSelected();
                this.selectItem(items.get(nodes.indexOf(node)));
                
                borderThickness.adjustValue(rect.getBorderThickness());
                
                focusAngle.adjustValue(rect.getFocusAngle());
                focusDistance.adjustValue(rect.getFocusDistance());
                centerX.adjustValue(rect.getCenterX());
                centerY.adjustValue(rect.getCenterY());
                radius.adjustValue(rect.getRadius());
                cycleMethod.setValue(rect.getCycleMethod());
                colorPickerStop0.setValue(rect.getStop0Color());
                colorPickerStop1.setValue(rect.getStop1Color());
                
                if(x.getClickCount()==2&&items.get(this.getNodeIndex(node)).isTextField){                
                    itemsController.processEditText();
                }
            });
            
            rect.setOnMouseEntered(e->{
                if(isItemSelected()){
                    rect.setOnScroll((ScrollEvent w) ->{
                        double zoomFactor = 1.05;
                        double deltaY = w.getDeltaY();
                        if (deltaY < 0){
                            zoomFactor = 2.0 - zoomFactor;
                    }
                        rect.setScaleX(rect.getScaleX() * zoomFactor);
                        rect.setScaleY(rect.getScaleY() * zoomFactor);
                    });
                }
            });
        
            rect.setOnMousePressed(e->{
                this.clearSelected();
                this.selectedNode = rect;
                Glow glow = new Glow();
                rect.setEffect(glow);
                this.selectItem(items.get(nodes.indexOf(node)));
                orgSceneX = e.getSceneX();
                orgSceneY = e.getSceneY();
                orgTranslateX = rect.getTranslateX();
                orgTranslateY = rect.getTranslateY();
            });
        
            rect.setOnMouseDragged(e->{
                double offsetX = e.getSceneX() - orgSceneX;
                double offsetY = e.getSceneY() - orgSceneY;
                double newTranslateX = orgTranslateX + offsetX;
                double newTranslateY = orgTranslateY + offsetY;

                rect.setTranslateX(newTranslateX);
                rect.setTranslateY(newTranslateY);
                rect.setOnMouseReleased(x->{
                    rect.setEffect(null);
                    DragAndMove_Transaction transaction = new DragAndMove_Transaction(rect, orgTranslateX, orgTranslateY, newTranslateX, newTranslateY);
                    app.processTransaction(transaction);
                });
            });
        }
        
    }
    
    public void addImage(Node node){
        nodes.add(node);
        ItemsController itemsController = new ItemsController((GoLogoLoApp)app);
            ImageView image = (ImageView) node;
            
            image.setOnMouseClicked(x-> {
                if(x.getClickCount()==2&&items.get(this.getNodeIndex(node)).isTextField){                
                    itemsController.processEditText();
                }
            });
        
        
            image.setOnMousePressed(e->{
                orgSceneX = e.getSceneX();
                orgSceneY = e.getSceneY();
                orgTranslateX = image.getTranslateX();
                orgTranslateY = image.getTranslateY();
            });
        
            image.setOnMouseDragged(e->{
                double offsetX = e.getSceneX() - orgSceneX;
                double offsetY = e.getSceneY() - orgSceneY;
                double newTranslateX = orgTranslateX + offsetX;
                double newTranslateY = orgTranslateY + offsetY;

                image.setTranslateX(newTranslateX);
                image.setTranslateY(newTranslateY);
                image.setOnMouseReleased(x->{
                    DragAndMove_Transaction transaction = new DragAndMove_Transaction(image, orgTranslateX, orgTranslateY, newTranslateX, newTranslateY);
                    app.processTransaction(transaction);
                });
            });
    }
    
    public void addShape(Shape shape){
        nodes.add(shape);
        
    }
    
    public void removeNode(Node node) {
        nodes.remove(node);
    }
    
    public void removeNode(int index){
        nodes.remove(index);
    }
    
    public void selectItem(int index){
        this.selectItem(items.get(nodes.indexOf(index)));
    }

    public GoLogoLoItemPrototype getSelectedItem() {
        if (!isItemSelected()) {
            return null;
        }
        return getSelectedItems().get(0);
    }
    
   public ObservableList<GoLogoLoItemPrototype> getItems(){
       return items;
   }
    
    public ObservableList<GoLogoLoItemPrototype> getSelectedItems() {
        return (ObservableList<GoLogoLoItemPrototype>)this.itemsSelectionModel.getSelectedItems();
    }
    
    public ObservableList<Node> getSelectedComponents(){
        return (ObservableList<Node>)nodes;
    }
    
    public int getItemIndex(GoLogoLoItemPrototype item) {
        return items.indexOf(item);
    }
    
    public int getNodeIndex(Node node){
        return nodes.indexOf(node);
    }
    
    public Text getTextField(int index){
        return (Text)nodes.get(index);
    }
    
    public void addItemAt(GoLogoLoItemPrototype item, int itemIndex) {
        items.add(itemIndex, item);
    }

    public void moveItem(int oldIndex, int newIndex) {
        GoLogoLoItemPrototype itemToMove = items.remove(oldIndex);
        items.add(newIndex, itemToMove);
    }
    
    public void moveNode(int oldIndex, int newIndex){
        Node nodeToMove = nodes.remove(oldIndex);
        nodes.add(newIndex, nodeToMove);
    }

    public int getNumItems() {
        return items.size();
    }

    public void selectItem(GoLogoLoItemPrototype itemToSelect) {
        this.itemsSelectionModel.select(itemToSelect);
    }

    public ArrayList<Integer> removeAll(ArrayList<GoLogoLoItemPrototype> itemsToRemove) {
        ArrayList<Integer> itemIndices = new ArrayList();
        for (GoLogoLoItemPrototype item: itemsToRemove) {
            itemIndices.add(items.indexOf(item));
        }
        for (GoLogoLoItemPrototype item: itemsToRemove) {
            items.remove(item);
        }
        return itemIndices;
    }

    public void addAll(ArrayList<GoLogoLoItemPrototype> itemsToAdd, ArrayList<Integer> addItemLocations) {
        for (int i = 0; i < itemsToAdd.size(); i++) {
            GoLogoLoItemPrototype itemToAdd = itemsToAdd.get(i);
            Integer location = addItemLocations.get(i);
            items.add(location, itemToAdd);
        }
    }
    
    public ArrayList<GoLogoLoItemPrototype> getCurrentItemsOrder() {
        ArrayList<GoLogoLoItemPrototype> orderedItems = new ArrayList();
        for (GoLogoLoItemPrototype item : items) {
            orderedItems.add(item);
        }
        return orderedItems;
    }

    public void clearSelected() {
        this.itemsSelectionModel.clearSelection();
    }

    public void sortItems(Comparator sortComparator) {
        Collections.sort(items, sortComparator);
    }

    public void rearrangeItems(ArrayList<GoLogoLoItemPrototype> oldListOrder) {
        items.clear();
        for (GoLogoLoItemPrototype item : oldListOrder) {
            items.add(item);
        }
    }
 
    
    @Override
    public void reset(){
        
    }

    public void removeNodes(Node node, int index) {
        nodes.remove(node);
        tempNodes.add(node);
    }
    
    public void addNodes(){
        nodes.add(0, tempNodes.get(0));
        tempNodes.remove(0);
    }

    public Node getNode(int index) {
        return nodes.get(index);
    }
    
    public boolean isNodeSelected(){
        return this.isNodeSelected;
    }
    
    public Node getSelectedNode(){
        return this.selectedNode;
    }
    
    public GoLogoLoApp getApp(){
        return app;
    }
}
