/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data;

import java.util.List;
import javafx.scene.paint.Color;
import static javafx.scene.paint.Color.BLACK;
import static javafx.scene.paint.Color.WHITE;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;

/**
 *
 * @author qihong
 */
public class GoLogoLoCircle extends Circle{
    Circle circ;
    double borderThickness;
    double r;
    double xPos;
    double yPos;
    double focusAngle;
    double focusDistance;
    double centerX;
    double centerY;
    double radius;
    Color borderColor;boolean proportional;
    CycleMethod cycleMethod;
    List<Stop>stops;
    Stop stop0;
    Stop stop1;
    RadialGradient gradient;
    
    public GoLogoLoCircle(double r, double xPos, double yPos){
        circ = new Circle(xPos, yPos, r);
        circ.setStroke(BLACK);
        circ.setFill(WHITE);
        this.setXPos(xPos);
        this.setYPos(yPos);
        this.setBorderColor(BLACK);
        this.setStop0(new Stop(0,WHITE));
        this.setStop1(new Stop(1,WHITE));
        borderThickness = 1;
        focusAngle = 0;
        focusDistance = 0;
        centerX = 0;
        centerY = 0;
        radius = 0;
        borderColor = BLACK;
        proportional = true;
        stop0 = getStop0();
        stop1 = getStop1();
        cycleMethod = CycleMethod.NO_CYCLE;
    }
    
    public GoLogoLoCircle(double rad, double xPos, double yPos,
                                double borderT, Color borderC,
                                double focusA, double focusD, double cx, double cy, double r,
                                Stop stop_0, Stop stop_1, CycleMethod c
                                ){
        circ = new Circle(rad, xPos, yPos);
        circ.setStroke(BLACK);
        borderThickness = borderT;
        borderColor = borderC;
        focusAngle = focusA;
        focusDistance = focusD;
        centerX = cx;
        centerY = cy;
        radius = r;
        proportional = true;
        stop0 = stop_0;
        stop1 = stop_1;
        cycleMethod = c;
        gradient = new RadialGradient(focusA, focusD, 
                                          cx, cy,
                                            r, true, c, stop_0, stop_1);
    }
    
    public double getBorderThickness() {
        return borderThickness;
    }

    public void setBorderThickness(double borderThickness) {
        this.borderThickness = borderThickness;
    }
    public RadialGradient getGradient() {
        return gradient;
    }

    public void setGradient(RadialGradient gradient) {
        this.gradient = gradient;
    }
    
    public Color getBorderColor() {
        return borderColor;
    }
    
    public Color getStop0Color(){
        return stop0.getColor();
    }
    
    public Color getStop1Color(){
        return stop1.getColor();
    }

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }
    
    public Stop getStop0() {
        return stop0;
    }

    public void setStop0(Stop stop0) {
        this.stop0 = stop0;
    }

    public Stop getStop1() {
        return stop1;
    }

    public void setStop1(Stop stop1) {
        this.stop1 = stop1;
    }
    
    public List<Stop> getStops(){
        return stops;
    }
    
    public boolean isProportional() {
        return proportional;
    }

    public void setProportional(boolean proportional) {
        this.proportional = proportional;
    }
    
    public double getFocusAngle(){
        return focusAngle;
    }
    
    public void setFocusAngle(double value){
        focusAngle = value;
    }
    
    public double getFocusDistance() {
        return focusDistance;
    }

    public void setFocusDistance(double focusDistance) {
        this.focusDistance = focusDistance;
    }

    public double getRadialX() {
        return centerX;
    }

    public void setRadialX(double centerX) {
        this.centerX = centerX;
    }

    public double getRadialY() {
        return centerY;
    }

    public void setRadialY(double centerY) {
        this.centerY = centerY;
    }

    public double getRadialRadius() {
        return radius;
    }

    public void setRadialRadius(double radius) {
        this.radius = radius;
    }

    public CycleMethod getCycleMethod() {
        return cycleMethod;
    }

    public void setCycleMethod(CycleMethod cycleMethod) {
        this.cycleMethod = cycleMethod;
    }
    
    public double getXPos(){
        return xPos;
    }
    
    public double getYPos(){
        return yPos;
    }
    
    public void setXPos(double value){
        this.xPos = value;
    }
    
    public void setYPos(double value){
        this.yPos = value;
    }
    
    public Object clone() {
        return circ = new GoLogoLoCircle(r, xPos, yPos, 
                borderThickness, borderColor, focusAngle, focusDistance, centerX, centerY, radius,
                stop0, stop1, cycleMethod);
    }
    
    public boolean equals(Object obj) {
        return this == obj;
    }
}
