/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author Qihong
 */
public class GoLogoLoItemPrototype implements Cloneable{
    public static final int DEFAULT_ORDER = 0;
    public static final String DEFAULT_NAME = "?";
    public static final String DEFAULT_TYPE = "?";
    
    final IntegerProperty order;
    final StringProperty name;
    final StringProperty type;
    
    boolean isTextField;
    boolean isShape;
    boolean isCircle;
    boolean isImage;
    
    public GoLogoLoItemPrototype(){
        order = new SimpleIntegerProperty(DEFAULT_ORDER);
        name = new SimpleStringProperty(DEFAULT_NAME);
        type = new SimpleStringProperty(DEFAULT_TYPE);
    }
    
    public GoLogoLoItemPrototype(int initOrder, String initName, String initType){
        this();
        order.set(initOrder);
        name.set(initName);
        type.set(initType);
    }
    
    public GoLogoLoItemPrototype(int initOrder, String initName, String initType, boolean shape, boolean text, boolean circle, boolean image){
        this();
        order.set(initOrder);
        name.set(initName);
        type.set(initType);
        this.isShape = shape;
        this.isTextField = text;
        this.isCircle = circle;
        this.isImage = image;
    }

    public boolean isIsImage() {
        return isImage;
    }

    public void setIsImage(boolean isImage) {
        this.isImage = isImage;
    }
    
    public boolean isIsCircle() {
        return isCircle;
    }

    public void setIsCircle(boolean isCircle) {
        this.isCircle = isCircle;
    }
    
    public int getOrder(){
        return order.get();
    }
    
    public String getName(){
        return name.get();
    }
    
    public String getType(){
        return type.get();
    }
    
    public void setOrder(int value) {
        order.set(value);
    }
    
    public void setName(String value) {
        name.set(value);
    }
    
    public void setType(String value) {
        type.set(value);
    }
    
    public IntegerProperty orderProperty(){
        return order;
    }
    
    public StringProperty typeProperty(){
        return type;
    }
    
    public StringProperty nameProperty(){
        return name;
    }
    
    public boolean getIsText(){
        return isTextField;
    }
    
    public boolean getIsShape(){
        return isShape;
    }
    
    public void setIsText(boolean value){
        this.isTextField = value;
    }
    
    public void setIsShape(boolean value){
        this.isShape = value;
    }
    
    public Object clone() {
        return new GoLogoLoItemPrototype(   order.getValue(), 
                                        name.getValue(), 
                                        type.getValue(), getIsShape(), getIsText(), isIsCircle(), isIsImage());
    }
    
    public boolean equals(Object obj) {
        return this == obj;
    }
}
