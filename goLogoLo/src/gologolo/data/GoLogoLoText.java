/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.data;

import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_TEXT;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_DIALOG_TEXT_FIELD;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_RECTANGLE;
import static java.awt.Color.black;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import static javafx.scene.paint.Color.BLACK;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author qihong
 */
public class GoLogoLoText extends Text{
    Text text;
    String txt;
    Color color;
    double size;
    Font font;
    double width;
    double height;
    double xPos;
    double yPos;
    boolean Bold, Italic;
    public GoLogoLoText(String txt, double xPos, double yPos){
        text = new Text();
        this.txt = txt;
        this.setXPos(xPos);
        this.setYPos(yPos);
        font = new Font("Arial", 14);
        size = 14;
        color = BLACK;
        Bold = false;
        Italic = false;
    }
    
    public GoLogoLoText(String txt, double xPos, double yPos, Font fonts, Color color, boolean isBold, boolean isI){
        text = new Text();
        this.txt = txt;
        this.xPos = xPos;
        this.yPos = yPos;
        font = fonts;
        size = 14;
        this.color = color;
        Bold = isBold;
        Italic = isI;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    
    public boolean isBold() {
        return Bold;
    }

    public void setBold(boolean Bold) {
        this.Bold = Bold;
    }

    public boolean isItalic() {
        return Italic;
    }

    public void setItalic(boolean Italic) {
        this.Italic = Italic;
    }

    public String getTxt(){
        return txt;
    }
    
    public String getFontToString(){
        return font.getName();
    }
    
    public double getSize(){
        return size;
    }
    
    public double getXPos(){
        return xPos;
    }
    
    public double getYPos(){
        return yPos;
    }
    
    public void setXPos(double value){
        this.xPos = value;
        text.setX(xPos);
    }
    
    public void setFont(String value){
        font = new Font(value, getSize());
    }
    
    public void setfont(Font value){
        font = value;
    }
    
    public void setYPos(double value){
        this.yPos = value;
    }
    
    public void setTxt(String value){
        text.setText(value);
    }
    
    public void setSize(double value){
        this.size = value;
    }
    
    public Object clone(){
        return new GoLogoLoText(txt, xPos, yPos, font, color, Bold, Italic);
    }

    public Node getItSelfPlease() {
        return text;
    }
}
