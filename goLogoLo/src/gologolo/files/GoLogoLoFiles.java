/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gologolo.files;

import static djf.AppPropertyType.APP_EXPORT_PAGE;
import static djf.AppPropertyType.APP_PATH_EXPORT;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import static gologolo.GoLogoLoPropertyType.LOGO_MIDDLE_PANE;
import gologolo.data.GoLogoLoCircle;
import gologolo.data.GoLogoLoData;
import gologolo.data.GoLogoLoItemPrototype;
import gologolo.data.GoLogoLoRectangle;
import gologolo.data.GoLogoLoText;
import static gologolo.workspace.style.LogoStyle.CLASS_LOGO_RECTANGLE;
import static java.awt.SystemColor.text;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javafx.embed.swing.SwingFXUtils;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import javax.swing.text.html.HTML;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javax.imageio.ImageIO;
import javax.json.JsonObjectBuilder;
import org.xml.sax.SAXException;
import properties_manager.PropertiesManager;
/**
 *
 * @author Qihong
 */
public class GoLogoLoFiles implements AppFileComponent{
    // FOR JSON SAVING AND LOADING
    static final String JSON_ORDER = "order";
    static final String JSON_NAME = "name";
    static final String JSON_TYPE = "type";
    static final String JSON_ITEMS = "items";
    
    static final String JSON_NODES = "nodes";
    static final String JSON_WIDTH = "width";
    static final String JSON_HEIGHT = "height";
    static final String JSON_X = "x";
    static final String JSON_Y = "y";
    
    static final String JSON_BORDERTHICKNESS = "border_thickness";
    static final String JSON_BORDERRADIUS = "border_radius";
    static final String JSON_BORDERCOLOR = "border_color";
    
    static final String JSON_ANGLE = "angle";
    static final String JSON_DISTANCE = "distance";
    static final String JSON_CENTER_X = "center_x";
    static final String JSON_CENTER_Y = "center_y";
    static final String JSON_RADIALRADIUS = "radial_radius";
    static final String JSON_CYCLE = "cycle";
    static final String JSON_STOP0 = "stop0";
    static final String JSON_STOP1 = "stop1";
    
    static final String JSON_RADIUS = "radius";
    static final String JSON_TEXT = "text";
    
    static final String JSON_ISTEXT = "isText";
    static final String JSON_ISCIRCLE = "isCircle";
    static final String JSON_ISRECTANGLE = "isRectangle";
    
    static final String TITLE_TAG = "title";
    static final String OWNER_TAG = "list_owner_td";
    /**
     * This method is for saving user work.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	//Get the Data
        GoLogoLoData logoData = (GoLogoLoData)data;
	
	// NOW BUILD THE JSON ARRAY FOR THE LIST
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        Iterator<GoLogoLoItemPrototype> itemsIt = logoData.itemsIterator();
        Iterator<Node> nodesIt = (Iterator<Node>)logoData.nodesIterator();
        GoLogoLoRectangle rec;
        GoLogoLoText txt;
        GoLogoLoCircle cir;
	while (itemsIt.hasNext()) {
            GoLogoLoItemPrototype item = itemsIt.next();
            Node node = nodesIt.next();
            JsonObjectBuilder itemJson = Json.createObjectBuilder();
            if(item.getIsText()){
                txt = (GoLogoLoText) node;
                itemJson.add(JSON_ISRECTANGLE, item.getIsShape());
                itemJson.add(JSON_ISCIRCLE, item.isIsCircle());
                itemJson.add(JSON_ISTEXT, item.getIsText());
                itemJson.add(JSON_TEXT, txt.getText());
                itemJson.add(JSON_X, txt.getXPos());
                itemJson.add(JSON_Y, txt.getYPos());
            }
            else if(item.getIsShape()){
                rec = (GoLogoLoRectangle) node;
                itemJson.add(JSON_ISRECTANGLE, item.getIsShape());
                itemJson.add(JSON_ISCIRCLE, item.isIsCircle());
                itemJson.add(JSON_ISTEXT, item.getIsText());
                itemJson.add(JSON_X, rec.getXPos());
                itemJson.add(JSON_Y, rec.getYPos());
                itemJson.add(JSON_BORDERTHICKNESS, rec.getBorderThickness());
                itemJson.add(JSON_BORDERRADIUS, rec.getBorderRadius());
                itemJson.add(JSON_BORDERCOLOR, rec.getBorderColor().toString());
                itemJson.add(JSON_HEIGHT, rec.getHeight());
                itemJson.add(JSON_WIDTH, rec.getWidth());
                itemJson.add(JSON_ANGLE, rec.getFocusAngle());
                itemJson.add(JSON_DISTANCE, rec.getFocusDistance());
                itemJson.add(JSON_CENTER_X, rec.getCenterX());
                itemJson.add(JSON_CENTER_Y, rec.getCenterY());
                itemJson.add(JSON_RADIALRADIUS, rec.getRadius());
                itemJson.add(JSON_CYCLE, rec.getCycleMethod().toString());
                itemJson.add(JSON_STOP0, rec.getStop0().toString());
                itemJson.add(JSON_STOP1, rec.getStop1().toString());
            }
            else if(item.isIsCircle()){
                cir = (GoLogoLoCircle) node;
                itemJson.add(JSON_ISRECTANGLE, item.getIsShape());
                itemJson.add(JSON_ISCIRCLE, item.isIsCircle());
                itemJson.add(JSON_ISTEXT, item.getIsText());
                itemJson.add(JSON_X, cir.getCenterX());
                itemJson.add(JSON_Y, cir.getCenterY());
                itemJson.add(JSON_BORDERTHICKNESS, cir.getBorderThickness());
                itemJson.add(JSON_BORDERCOLOR, cir.getBorderColor().toString());
                itemJson.add(JSON_RADIUS, cir.getRadius());
                itemJson.add(JSON_ANGLE, cir.getFocusAngle());
                itemJson.add(JSON_DISTANCE, cir.getFocusDistance());
                itemJson.add(JSON_CENTER_X, cir.getRadialX());
                itemJson.add(JSON_CENTER_Y, cir.getRadialY());
                itemJson.add(JSON_RADIALRADIUS, cir.getRadialRadius());
                itemJson.add(JSON_CYCLE, cir.getCycleMethod().toString());
                itemJson.add(JSON_STOP0, cir.getStop0().toString());
                itemJson.add(JSON_STOP1, cir.getStop1().toString());
            }
                itemJson.add(JSON_ORDER, item.getOrder());
                itemJson.add(JSON_NAME, item.getName());
                itemJson.add(JSON_TYPE, item.getType());
            JsonObject itemJsonObj = itemJson.build();
	    arrayBuilder.add(itemJsonObj);
	}
        JsonArray itemsArray = arrayBuilder.build();
	JsonObject toDoDataJSO = Json.createObjectBuilder()
		.add(JSON_ITEMS, itemsArray)
		.build();
        
        
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
        
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(toDoDataJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error
     * reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
	GoLogoLoData logoData = (GoLogoLoData)data;
	logoData.reset();
	
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
	
	// AND NOW LOAD ALL THE ITEMS
	JsonArray jsonItemArray = json.getJsonArray(JSON_ITEMS);
	for (int i = 0; i < jsonItemArray.size(); i++) {
	    JsonObject jsonItem = jsonItemArray.getJsonObject(i); 
	    GoLogoLoItemPrototype item = loadItem(jsonItem);
            GoLogoLoRectangle rec;
            GoLogoLoCircle cir;
            GoLogoLoText text;
            if(item.isIsCircle()){
                cir = loadCircle(jsonItem);
                logoData.addNode(cir);
            }
            else if(item.getIsShape()){
                rec = loadRec(jsonItem);
                logoData.addNode(rec);
            }
            else{
                text = loadText(jsonItem);
                logoData.addNode(text);
            }
	    logoData.addItem(item);
	}
    }
    
    /**
     * This method would be used to export data to another format,
     * which we're not doing in this assignment.
     */
    @Override
    public void exportData(AppDataComponent data, String savedFileName) throws IOException {
        GoLogoLoData logoData = (GoLogoLoData)data;
        String logoName = savedFileName.substring(0);
        WritableImage image = logoData.getApp().getGUIModule().getGUINode(LOGO_MIDDLE_PANE).snapshot(new SnapshotParameters(), null);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String exportDirPath = props.getProperty(APP_PATH_EXPORT) + logoName + ".png" + "/";

        // TODO: probably use a file chooser here
        File file = new File(exportDirPath);

        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException e) {
            // TODO: handle exception here
        }
    }
    
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        
    }
    
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    public GoLogoLoRectangle loadRec(JsonObject jsonNode){
        double width = jsonNode.getInt(JSON_WIDTH);
        double height = jsonNode.getInt(JSON_HEIGHT);
        double xPos = jsonNode.getInt(JSON_X);
        double yPos = jsonNode.getInt(JSON_Y);
        double borderThickness = Double.parseDouble(jsonNode.getString(JSON_BORDERTHICKNESS));
        double borderRadius = Double.parseDouble(jsonNode.getString(JSON_BORDERCOLOR));
        Color borderColor = Color.valueOf(jsonNode.getString(JSON_BORDERCOLOR));
        double focusAngle = Double.parseDouble(jsonNode.getString(JSON_ANGLE));
        double focusDistance = Double.parseDouble(jsonNode.getString(JSON_DISTANCE));
        double centerX = Double.parseDouble(jsonNode.getString(JSON_CENTER_X));
        double centerY = Double.parseDouble(jsonNode.getString(JSON_CENTER_Y));
        double radialRadius = Double.parseDouble(jsonNode.getString(JSON_RADIALRADIUS));
        CycleMethod cycle = CycleMethod.valueOf(jsonNode.getString(JSON_CYCLE));
        Color stop0 = Color.valueOf(jsonNode.getString(JSON_STOP0));
        Color stop1 = Color.valueOf(jsonNode.getString(JSON_STOP1));
        GoLogoLoRectangle rect = new GoLogoLoRectangle(width,height,xPos,yPos);
        rect.setFill(new RadialGradient(focusAngle, focusDistance, centerX, centerY, radialRadius, true, cycle, new Stop(0,stop0), new Stop(1,stop1)));
        rect.setStroke(borderColor);
        rect.setStrokeWidth(borderThickness);
        rect.setArcHeight(borderRadius);
        rect.setArcWidth(borderRadius);
        return rect;
    }
    public GoLogoLoText loadText(JsonObject jsonNode){
        int xPos = jsonNode.getInt(JSON_X);
        int yPos = jsonNode.getInt(JSON_Y);
        String text = jsonNode.getString(JSON_TEXT);
        GoLogoLoText textt = new GoLogoLoText(text, xPos, yPos);
        return textt;
    }
    public GoLogoLoItemPrototype loadItem(JsonObject jsonItem) {
	// GET THE DATA
	int order = jsonItem.getInt(JSON_ORDER);
	String name = jsonItem.getString(JSON_NAME);
        String type = jsonItem.getString(JSON_TYPE);
        String isShape = jsonItem.getString(JSON_ISRECTANGLE);
        String isCircle = jsonItem.getString(JSON_ISCIRCLE);
        String isText = jsonItem.getString(JSON_ISTEXT);
        
	// THEN USE THE DATA TO BUILD AN ITEM
        GoLogoLoItemPrototype item = new GoLogoLoItemPrototype(order, name, type);
        if(isShape.equals("true")){
            item.setIsShape(true);
            item.setIsCircle(false);
            item.setIsText(false);
        }
        else if (isText.equals("true")){
            item.setIsText(true);
            item.setIsShape(false);
            item.setIsCircle(false);
        }
        else{
            item.setIsShape(false);
            item.setIsCircle(true);
            item.setIsText(false);
        }
	// ALL DONE, RETURN IT
	return item;
    }

    private GoLogoLoCircle loadCircle(JsonObject jsonNode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
